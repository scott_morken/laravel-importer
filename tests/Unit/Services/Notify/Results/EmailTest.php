<?php


namespace Smorken\Importer\tests\Unit\Services\Notify\Results;


use Exception;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Mail\Mailer;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportRun;
use Smorken\Importer\Mail\ImportResults;
use Smorken\Importer\Services\Notify\Results\Email;

class EmailTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testNoEmailToDoesntSendAndReturnsTrue()
    {
        [$sut, $m, $c, $l, $i, $ir] = $this->getSut();
        $i->email_to = null;
        $this->assertTrue($sut->send($i, $ir));
    }

    public function testSendIsTrue()
    {
        [$sut, $m, $c, $l, $i, $ir] = $this->getSut();
        $c->shouldReceive('get')->once()->with('importer.from')->andReturn('from@example.org');
        $i->email_to = 'foo@example.org';
        $i->descr = 'Foo Import';
        $m->shouldReceive('to->send')->once()->with(
            m::type(ImportResults::class)
        )->andReturn(true);
        $this->assertTrue($sut->send($i, $ir));
    }

    public function testSendWithExceptionLogsErrorAndIsFalse()
    {
        [$sut, $m, $c, $l, $i, $ir] = $this->getSut();
        $c->shouldReceive('get')->once()->with('importer.from')->andReturn('from@example.org');
        $i->email_to = 'foo@example.org';
        $i->descr = 'Foo Import';
        $except = new Exception('Test Exception');
        $m->shouldReceive('to->send')->once()->with(
            m::type(ImportResults::class)
        )
          ->andThrow($except);
        $l->shouldReceive('report')->once()->with($except);
        $this->assertFalse($sut->send($i, $ir));
    }

    protected function getSut()
    {
        $i = m::mock(Import::class);
        $ir = m::mock(ImportRun::class);
        $m = m::mock(Mailer::class);
        $c = m::mock(Repository::class);
        $l = m::mock(ExceptionHandler::class);
        $sut = new Email($m, $c, $l);
        return [$sut, $m, $c, $l, $i, $ir];
    }
}
