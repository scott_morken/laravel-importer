<?php


namespace Smorken\Importer\Tests\Unit\Import\Cleanup\HR;


class WithTrashedModel
{

    public function trashed()
    {
        return true;
    }

    public function withTrashed()
    {
        return true;
    }
}
