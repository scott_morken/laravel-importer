<?php


namespace Smorken\Importer\Tests\Unit\Import\Cleanup\HR;


use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Storage\HR\Department;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\Job;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Location;
use Smorken\Importer\Contracts\Storage\HR\Person;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Contracts\Storage\HR\Type;
use Smorken\Importer\Import\Cleanup\HR\Handler;

class HandlerTest extends TestCase
{

    public function setUp(): void
    {
        ini_set('date.timezone', 'UTC');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCleanupForcedAndForceDeleteExists()
    {
        $query = m::mock(new ForceDeleteQuery());
        $model = m::mock(new WithTrashedModel());
        [$sut, $providers] = $this->getSut($query, $model);
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $this->staleIds($p, [1], true);
        }
        $r = $sut->cleanup(true);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 4,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 1,
            ],
            'delete' => [
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupForcedAndForceDeleteNotExistsIsNormalDelete()
    {
        [$sut, $providers] = $this->getSut();
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $this->staleIds($p, [1]);
        }
        $r = $sut->cleanup(true);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'forceDelete' => [],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 4,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupForcedAndForceDeleteWithoutTrashedOverridesForced()
    {
        $query = m::mock(new ForceDeleteQuery());
        [$sut, $providers] = $this->getSut($query);
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $this->staleIds($p, [1], true);
        }
        $r = $sut->cleanup(true);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,//exists as core and related queries
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 4,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupNotForcedNoStale()
    {
        [$sut, $providers] = $this->getSut();
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $this->staleIds($p, []);
        }
        $r = $sut->cleanup(false);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupNotForcedOneResult()
    {
        [$sut, $providers] = $this->getSut();
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $this->staleIds($p, [1]);
        }
        $r = $sut->cleanup(false);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'forceDelete' => [],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 1,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 4,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    protected function getProviders($stub_query, $stub_model)
    {
        $clses = [
            Person::class,
            JobPerson::class,
            Email::class,
            Phone::class,
            Type::class,
            Job::class,
            Department::class,
            Location::class,
        ];
        $providers = [];
        foreach ($clses as $c) {
            $p = m::mock($c);
            $model = $stub_model ?: m::mock($c.'_MODEL');
            $p->shouldReceive('getModel')->andReturn($model);
            $q = $stub_query ?: m::mock($c.'Query');
            $model->shouldReceive('newQuery')->andReturn($q);
            $model->shouldReceive('getKeyName')->andReturn('id');
            $providers[$c] = $p;
        }
        return $providers;
    }

    protected function getStaleCollection($ids)
    {
        $coll = new Collection();
        foreach ($ids as $id) {
            $m = m::mock('StaleModel');
            $m->id = $id;
            $coll->push($m);
        }
        return $coll;
    }

    protected function getSut($stub_query = false, $stub_model = false)
    {
        $sut = new Handler();
        $providers = $this->getProviders($stub_query, $stub_model);
        return [$sut, $providers];
    }

    protected function staleIds($provider, $ids = [], $allow_force = false)
    {
        $m = $provider->getModel();
        $q = $m->newQuery();
        $q->shouldReceive('where')->with('updated_at', '<', m::any())->andReturn($q);
        $q->shouldReceive('get')->andReturn($this->getStaleCollection($ids));
        $q->shouldReceive('whereIn')->with(m::any(), $ids)->andReturn($q);
        $q->shouldReceive('delete')->andReturn(count($ids));
        if ($allow_force) {
            $q->shouldReceive('forceDelete')->andReturn(count($ids));
        }
    }
}
