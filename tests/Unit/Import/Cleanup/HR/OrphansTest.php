<?php


namespace Smorken\Importer\Tests\Unit\Import\Cleanup\HR;


use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Import\Cleanup\HR\Orphans;

class OrphansTest extends TestCase
{

    public function setUp(): void
    {
        ini_set('date.timezone', 'UTC');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCleanupForced()
    {
        $stub_query = m::mock(new ForceDeleteQuery());
        $stub_model = m::mock(new WithTrashedModel());
        $stub_query->shouldReceive('forceDelete')->andReturn(0);
        [$sut, $providers] = $this->getSut($stub_query, $stub_model);
        $sut->setProviders($providers);
        $r = $sut->cleanup(true);
        $expected = [
            'stale' => [

            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
            ],
            'delete' => [

            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupNotForced()
    {
        [$sut, $providers] = $this->getSut();
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $q = $p->getModel()->newQuery();
            $this->mockDelete($q, 0);
        }
        $r = $sut->cleanup(false);
        $expected = [
            'stale' => [

            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testCleanupNotForcedWithCounts()
    {
        [$sut, $providers] = $this->getSut();
        $sut->setProviders($providers);
        foreach ($providers as $p) {
            $q = $p->getModel()->newQuery();
            $this->mockDelete($q, 1);
        }
        $r = $sut->cleanup(false);
        $expected = [
            'stale' => [

            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 1,
            ],
        ];
        $this->assertEquals($expected, $r);
    }

    protected function getProviders($stub_query, $stub_model)
    {
        $clses = [
            JobPerson::class,
            Email::class,
            Phone::class,
        ];
        $providers = [];
        foreach ($clses as $c) {
            $p = m::mock($c);
            $model = $stub_model ?: m::mock($c.'Model');
            $p->shouldReceive('getModel')->andReturn($model);
            $q = $stub_query ?: m::mock($c.'Query');
            $model->shouldReceive('newQuery')->andReturn($q);
            $q->shouldReceive('where')->with(
                m::on(
                    function ($where) {
                        $q = m::mock('Subquery');
                        $q->shouldReceive('orHas')->atLeast()->once();
                        $where($q);
                        return is_callable($where);
                    }
                )
            )->andReturn($q);
            $providers[$c] = $p;
        }
        return $providers;
    }

    protected function getSut($stub_query = false, $stub_model = false)
    {
        $sut = new Orphans();
        $providers = $this->getProviders($stub_query, $stub_model);
        return [$sut, $providers];
    }

    protected function mockDelete($query, $return = 0)
    {
        $query->shouldReceive('delete')->andReturn($return);
    }
}
