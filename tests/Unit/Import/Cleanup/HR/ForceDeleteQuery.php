<?php


namespace Smorken\Importer\Tests\Unit\Import\Cleanup\HR;


class ForceDeleteQuery
{

    public function forceDelete()
    {
        return true;
    }

    public function withTrashed()
    {
        return true;
    }
}
