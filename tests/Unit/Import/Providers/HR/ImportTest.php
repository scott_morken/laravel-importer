<?php


namespace Smorken\Importer\Tests\Unit\Import\Providers\HR;


use Faker\Factory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Storage\HR\Department;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\Job;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Location;
use Smorken\Importer\Contracts\Storage\HR\Person;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Contracts\Storage\HR\Type;
use Smorken\Importer\Import\Providers\HR\Import;
use Smorken\Importer\Models\VO\ImportResult;

class ImportTest extends TestCase
{

    public function setUp(): void
    {
        ini_set('date.timezone', 'UTC');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testImport()
    {
        $this->mockProvider(Person::class);
        $this->mockProvider(Location::class);
        $this->mockProvider(Department::class);
        $this->mockProvider(Type::class);
        $this->mockProvider(Job::class);
        $this->mockProvider(JobPerson::class);
        $this->mockProvider(Email::class);
        $this->mockProvider(Phone::class);
        $import = m::mock(\Smorken\Importer\Contracts\Models\Import::class);
        $import->id = 1;
        [$sut, $hr, $db] = $this->getSut();
        $hr->shouldReceive('getActiveEmployees')
           ->once()
           ->with(
               m::on(
                   function ($closure) {
                       $closure($this->getPeople());
                       return true;
                   }
               )
           );
        $db->shouldReceive('connection')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Person_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Location_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Department_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Type_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Job_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\JobPerson_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Email_TABLE')->andReturn($db);
        $db->shouldReceive('table')->with('Smorken\Importer\Contracts\Storage\HR\Phone_TABLE')->andReturn($db);
        $db->shouldReceive('createOrUpdate')->with(m::type('array'), m::type('array'), m::type('array'))->andReturn(1);
        $result = $sut->import($import)->toArray();
        $expected = [
            'person::total' => 1,
            'location::total' => 1,
            'department::total' => 1,
            'job::total' => 1,
            'job_person::total' => 1,
            'type::total' => 1,
            'email::total' => 1,
            'phone::total' => 1,
            'person::updated_or_created' => 1,
            'location::updated_or_created' => 1,
            'department::updated_or_created' => 1,
            'job::updated_or_created' => 1,
            'job_person::updated_or_created' => 1,
            'type::updated_or_created' => 1,
            'email::updated_or_created' => 1,
            'phone::updated_or_created' => 1,
            'errors' => 0,
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $result[$k]);
        }
    }

    protected function getPeople($count = 1)
    {
        $people = new Collection();
        $gen = Factory::create();
        for ($i = 0; $i < $count; $i++) {
            $people->push(new Stub($gen));
        }
        return $people;
    }

    protected function getSut()
    {
        $hrp = m::mock('HRProvider');
        $sut = new Import($hrp);
        $result = new ImportResult();
        $db = m::mock(DatabaseManager::class);
        $sut->init($result, $db);
        return [$sut, $hrp, $db];
    }

    protected function mockProvider($provider_cls)
    {
        $p = m::mock($provider_cls);
        $m = m::mock($provider_cls.'_MODEL');
        $p->shouldReceive('getModel')->andReturn($m);
        $m->shouldReceive('getTable')->andReturn($provider_cls.'_TABLE');
        $m->shouldReceive('getConnectionName')->andReturnNull();
        App::shouldReceive('make')->with($provider_cls)->andReturn($p);
    }
}
