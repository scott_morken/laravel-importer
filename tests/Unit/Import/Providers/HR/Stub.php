<?php


namespace Smorken\Importer\Tests\Unit\Import\Providers\HR;


use Faker\Generator;
use Illuminate\Support\Str;

class Stub
{
    protected $attributes = [
        'id' => null,
        'first_name' => null,
        'last_name' => null,
        'department_id' => null,
        'department' => null,
        'type_id' => null,
        'job_type' => null,
        'job_id' => null,
        'job_desc' => null,
        'manages_department_id' => null,
        'supervisor_id' => null,
        'email' => null,
        'phone' => null,
        'location_id' => null,
        'location' => null,
    ];

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * HrStub constructor.
     * @param  Generator  $faker
     * @param  array  $overrides
     */
    public function __construct(Generator $faker, $overrides = [])
    {
        $this->faker = $faker;
        $this->init($overrides);
    }

    public function __get($k)
    {
        if (isset($this->attributes[$k])) {
            return $this->attributes[$k];
        }
    }

    protected function getAltIds($count)
    {
        $alts = [];
        for ($i = 0; $i < $count; $i++) {
            $alts[] = Str::random(5).$this->faker->randomNumber(5);
        }
        return $alts;
    }

    protected function getDeptIds($count)
    {
        return $this->getKeys('DEPTID', $count);
    }

    protected function getIds($count)
    {
        return $this->getKeys('PERSID', $count);
    }

    protected function getJobIds($count)
    {
        return $this->getKeys('JOBID', $count);
    }

    protected function getKeys($key, $count)
    {
        $keys = [];
        for ($i = 0; $i < $count; $i++) {
            $keys[] = sprintf('%s-%d', $key, $i);
        }
        return $keys;
    }

    protected function getLocIds($count)
    {
        return $this->getKeys('LOCID', $count);
    }

    protected function getTypeIds($count)
    {
        return $this->getKeys('TYPEID', $count);
    }

    protected function init($overrides)
    {
        $this->attributes['id'] = $this->faker->randomElement($this->getIds(10));
        $this->attributes['first_name'] = $this->faker->firstName;
        $this->attributes['last_name'] = $this->faker->lastName;
        $this->attributes['department_id'] = $this->faker->randomElement($this->getDeptIds(5));
        $this->attributes['department'] = 'DEPT: '.$this->faker->words(2, true);
        $this->attributes['type_id'] = $this->faker->randomElement($this->getTypeIds(3));
        $this->attributes['job_type'] = 'TYPE: '.$this->faker->words(2, true);
        $this->attributes['job_id'] = $this->faker->randomElement($this->getJobIds(10));
        $this->attributes['job_desc'] = 'JOB: '.$this->faker->words(2, true);
        $this->attributes['manages_department_id'] = $this->faker->randomElement($this->getDeptIds(5));
        $this->attributes['supervisor_id'] = $this->faker->randomElement($this->getIds(10));
        $this->attributes['email'] = $this->faker->email;
        $this->attributes['phone'] = $this->faker->phoneNumber;
        $this->attributes['location_id'] = $this->faker->randomElement($this->getLocIds(2));
        $this->attributes['location'] = 'LOC: '.$this->faker->words(2, true);

        foreach ($overrides as $k => $v) {
            $this->attributes[$k] = $v;
        }
    }
}
