<?php


namespace Smorken\Importer\Tests\Unit\Import;


use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportProvider;
use Smorken\Importer\Contracts\Notify\Results;
use Smorken\Importer\Contracts\Storage\ImportRun;
use Smorken\Importer\Import\Runner;
use Smorken\Importer\Models\VO\ImportResult;

class RunnerTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testRunWithResult()
    {
        $import = m::mock(Import::class);
        $import->importProvider = m::mock(ImportProvider::class);
        $import->importProvider->provider = 'FooProviderClass';
        $fooProvider = m::mock('FooProviderClass');
        App::shouldReceive('make')->with('FooProviderClass', [])->andReturn($fooProvider);
        [$sut, $rp, $rn, $db] = $this->getSut();
        $resultModel = new ImportResult();
        $rp->shouldReceive('getResultModel')->once()->with(true)->andReturn($resultModel);
        $fooProvider->shouldReceive('init')->once()->with(m::type(\Smorken\Importer\Contracts\Models\ImportResult::class), $db);
        $fooProvider->shouldReceive('import')->once()->with($import)->andReturn($resultModel);
        $importRun = m::mock(\Smorken\Importer\Contracts\Models\ImportRun::class);
        $rp->shouldReceive('saveByImportAndResult')->once()->with($import, m::type(\Smorken\Importer\Contracts\Models\ImportResult::class))->andReturn($importRun);
        $rn->shouldReceive('send')->once()->with($import, $importRun);
        $this->assertEquals($resultModel, $sut->run($import));
    }

    protected function getSut()
    {
        $rp = m::mock(ImportRun::class);
        $rn = m::mock(Results::class);
        $db = m::mock(DatabaseManager::class);
        $sut = new Runner($rp, $rn, $db);
        return [$sut, $rp, $rn, $db];
    }
}
