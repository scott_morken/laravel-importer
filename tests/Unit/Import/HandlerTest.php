<?php


namespace Smorken\Importer\Tests\Unit\Import;


use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Importer\Contracts\Import\Runner;
use Smorken\Importer\Contracts\Storage\Import;
use Smorken\Importer\Import\Handler;

class HandlerTest extends TestCase
{

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testHandleMultipleImports()
    {
        [$sut, $r, $ip] = $this->getSut();
        $imports = $this->createImports(2);
        $ip->shouldReceive('getActiveImports')->once()->andReturn($imports);
        $r->shouldReceive('run')->once()->with($imports[0])->andReturn('foo');
        $r->shouldReceive('run')->once()->with($imports[1])->andReturn('bar');
        $c = $sut->handle();
        $this->assertInstanceOf(Collection::class, $c);
        $this->assertTrue($c->has('id-1'));
        $this->assertTrue($c->has('id-2'));
        $this->assertEquals('foo', $c->get('id-1'));
        $this->assertEquals('bar', $c->get('id-2'));
    }

    public function testHandleMultipleImportsHandleOneById()
    {
        [$sut, $r, $ip] = $this->getSut();
        $imports = $this->createImports(2);
        $ip->shouldReceive('find')->once()->with(1)->andReturn($imports[0]);
        $r->shouldReceive('run')->once()->with($imports[0])->andReturn('foo');
        $c = $sut->handle(1);
        $this->assertInstanceOf(Collection::class, $c);
        $this->assertCount(1, $c);
        $this->assertTrue($c->has('id-1'));
        $this->assertEquals('foo', $c->get('id-1'));
    }

    public function testHandleNoImport()
    {
        [$sut, $r, $ip] = $this->getSut();
        $ip->shouldReceive('getActiveImports')->once()->andReturn([]);
        $this->assertEmpty($sut->handle());
    }

    public function testHandleOneImport()
    {
        [$sut, $r, $ip] = $this->getSut();
        $imports = $this->createImports();
        $ip->shouldReceive('getActiveImports')->once()->andReturn($imports);
        $r->shouldReceive('run')->with($imports[0])->andReturn('foo');
        $c = $sut->handle();
        $this->assertInstanceOf(Collection::class, $c);
        $this->assertTrue($c->has('id-1'));
        $this->assertEquals('foo', $c->get('id-1'));
    }

    protected function createImports($count = 1)
    {
        $imports = [];
        for ($i = 0; $i < $count; $i++) {
            $import = m::mock(\Smorken\Importer\Contracts\Models\Import::class);
            $import->id = sprintf('id-%d', $i + 1);
            $imports[] = $import;
        }
        return $imports;
    }

    protected function getSut()
    {
        $runner = m::mock(Runner::class);
        $ip = m::mock(Import::class);
        $sut = new Handler($runner, $ip);
        return [$sut, $runner, $ip];
    }
}
