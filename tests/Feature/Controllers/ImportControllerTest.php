<?php


namespace Smorken\Importer\Tests\Feature\Controllers;


use Smorken\Importer\Models\Eloquent\Import;
use Smorken\Importer\Tests\TestCase;
use Smorken\Importer\Tests\Traits\Data;
use Smorken\Importer\Tests\Traits\Trim;

class ImportControllerTest extends TestCase
{

    use Trim, Data;

    public function testBaseRoute()
    {
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->see('Importer Administration')
             ->see('No records found.');
    }

    public function testBaseRouteWithRecords()
    {
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->see('Importer Administration')
             ->see($i->descr);
    }

    public function testCreateSuccessfully()
    {
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->click('New')
             ->seePageIs('/admin/importer/create')
             ->see('Create new record')
             ->type('Test Import', 'descr')
             ->select('hr', 'provider_id')
             ->type('foo@example.org', 'email_to')
             ->check('active')
             ->press('Save')
             ->seePageIs('/admin/importer')
             ->see('Test Import');
    }

    public function testCreateWithValidationErrors()
    {
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->click('New')
             ->seePageIs('/admin/importer/create')
             ->see('Create new record')
             ->press('Save')
             ->seePageIs('/admin/importer/create')
             ->see('descr field is required');
    }

    public function testDeleteRemovesRecord()
    {
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->see($i->descr)
             ->click('delete')
             ->seePageIs('/admin/importer/delete/'.$i->id)
             ->see('Delete record ['.$i->id.']')
             ->see($i->descr)
             ->press('Delete')
             ->seePageIs('/admin/importer')
             ->dontSee($i->descr);
    }

    public function testUpdateSuccessfully()
    {
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->see($i->descr)
             ->click('update')
             ->type('Update Import', 'descr')
             ->press('Save')
             ->seePageIs('/admin/importer')
             ->see('Update Import');
    }

    public function testViewCanRunImport()
    {
        $this->createFakeCompany();
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $response = $this->actingAs($this->mockAuth())
                         ->visit('/admin/importer')
                         ->click($i->id)
                         ->seePageIs('/admin/importer/view/'.$i->id)
                         ->click('Import now')
                         ->seePageIs('/admin/importer/run-view/'.$i->id)
            ->response->getContent();
        $expects = '<tr> <td>person::total</td> <td>10</td> </tr> '.
            '<tr> <td>location::total</td> <td>1</td> </tr> '.
            '<tr> <td>department::total</td> <td>3</td> </tr> '.
            '<tr> <td>job::total</td> <td>6</td> </tr> '.
            '<tr> <td>job_person::total</td> <td>11</td> </tr> '.
            '<tr> <td>type::total</td> <td>3</td> </tr> '.
            '<tr> <td>email::total</td> <td>11</td> </tr> '.
            '<tr> <td>phone::total</td> <td>11</td> </tr> '.
            '<tr> <td>person::updated_or_created</td> <td>10</td> </tr> '.
            '<tr> <td>location::updated_or_created</td> <td>1</td> </tr> '.
            '<tr> <td>department::updated_or_created</td> <td>3</td> </tr> '.
            '<tr> <td>job::updated_or_created</td> <td>6</td> </tr> '.
            '<tr> <td>job_person::updated_or_created</td> <td>11</td> </tr> '.
            '<tr> <td>type::updated_or_created</td> <td>3</td> </tr> '.
            '<tr> <td>email::updated_or_created</td> <td>11</td> </tr> '.
            '<tr> <td>phone::updated_or_created</td> <td>11</td> </tr> '.
            '<tr> <td>errors</td> <td>0</td> </tr>';
        $this->assertStringContainsString($expects, $this->trim($response));
    }

    public function testViewCanRunSample()
    {
        $this->createFakeCompany();
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $response = $this->actingAs($this->mockAuth())
                         ->visit('/admin/importer')
                         ->click($i->id)
                         ->seePageIs('/admin/importer/view/'.$i->id)
                         ->click('Sample data')
                         ->seePageIs('/admin/importer/sample/'.$i->id)
            ->response->getContent();
        $expects = [
            '[ persons ](10)',
            '[ M1 ](6)',
            '[ locations ](1)',
            '[ L-1 ](5) primary_id: L-1 name:',
            '[ depts ](3)',
            '[ D-0 ](5) primary_id: D-0 name:',
            '[ types ](3)',
            '[ T-3 ](5) primary_id: T-3 name:',
            '[ jobs ](6)',
            '[ J-M1 ](6) primary_id: J-M1 type_id: T-3 name:',
            '[ dept_persons ](0) - empty -',
            '[ job_persons ](11)',
            '[ M1-D-0-L-1-J-M1 ](9) person_id: M1 department_id: D-0 job_id: J-M1 supervisor_id: PRES location_id: L-1 manages: false',
            '[ emails ](11)',
            '[ phones ](11)',
        ];
        $trimmed = $this->trim($response);
        foreach ($expects as $str) {
            $this->assertStringContainsString($str, $trimmed);
        }
    }

    public function testViewShowsRecord()
    {
        $i = factory(Import::class)->create(['provider_id' => 'hr']);
        $this->actingAs($this->mockAuth())
             ->visit('/admin/importer')
             ->click($i->id)
             ->seePageIs('/admin/importer/view/'.$i->id)
             ->see('View record ['.$i->id.']')
             ->see($i->descr)
             ->see('No import run results');
    }
}
