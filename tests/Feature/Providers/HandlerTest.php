<?php


namespace Smorken\Importer\Tests\Feature\Providers;


use Smorken\Importer\Contracts\Import\Handler;
use Smorken\Importer\Models\Eloquent\Import;
use Smorken\Importer\Tests\TestCase;

class HandlerTest extends TestCase
{

    public function testWithImportAndEmptyResult()
    {
        $sut = $this->getSut();
        $this->createImport();
        $r = $sut->handle();
        $this->assertCount(1, $r);
        $expected = [
            'person::total' => 0,
            'location::total' => 0,
            'department::total' => 0,
            'job::total' => 0,
            'job_person::total' => 0,
            'type::total' => 0,
            'email::total' => 0,
            'phone::total' => 0,
            'person::updated_or_created' => 0,
            'location::updated_or_created' => 0,
            'department::updated_or_created' => 0,
            'job::updated_or_created' => 0,
            'job_person::updated_or_created' => 0,
            'type::updated_or_created' => 0,
            'email::updated_or_created' => 0,
            'phone::updated_or_created' => 0,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r->first()->getCounters());
    }

    protected function createImport()
    {
        return factory(Import::class)->create(['provider_id' => 'hr']);
    }

    /**
     * @return Handler
     */
    protected function getSut()
    {
        return $this->app[Handler::class];
    }
}
