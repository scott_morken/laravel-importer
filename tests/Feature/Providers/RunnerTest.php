<?php


namespace Smorken\Importer\Tests\Feature\Providers;


use Smorken\Importer\Contracts\Import\Runner;
use Smorken\Importer\Models\Eloquent\HR\Department;
use Smorken\Importer\Models\Eloquent\HR\Job;
use Smorken\Importer\Models\Eloquent\HR\Person;
use Smorken\Importer\Models\Eloquent\Import;
use Smorken\Importer\Models\VO\ImportProvider;
use Smorken\Importer\Tests\Feature\Stubs\Models\HR;
use Smorken\Importer\Tests\TestCase;
use Smorken\Importer\Tests\Traits\Data;
use Smorken\Importer\Tests\Traits\EnsureDatabase;

class RunnerTest extends TestCase
{

    use Data, EnsureDatabase;

    public function setUp(): void
    {
        $this->ensureTestingDatabase();
        parent::setUp();
    }

    public function testSampleWithEmpty()
    {
        $sut = $this->getSut();
        $import = $this->makeImport();
        $r = $sut->sample($import);
        $expected = [];
        $this->assertEquals($expected, $r);
    }

    public function testSampleWithFakeCompany()
    {
        $sut = $this->getSut();
        $import = $this->makeImport();
        $this->createFakeCompany();
        $r = $sut->sample($import);
        $expected = [
            'persons' => [
                'M1' => [],
                'M2' => [],
                'M3' => [],
                'M4' => [],
                'M5' => [],
                'PRES' => [],
                'S1' => [],
                'S2' => [],
                'VP1' => [],
                'VP2' => [],
            ],
            'locations' => [
                'L-1' => [],
            ],
            'depts' => [
                'D-0' => [],
                'D-1' => [],
                'D-2' => [],
            ],
            'types' => [
                'T-3' => [],
                'T-1' => [],
                'T-2' => [],
            ],
            'jobs' => [
                'J-M1' => [],
                'J-M2' => [],
                'J-P1' => [],
                'J-S1' => [],
                'J-VP1' => [],
                'J-VP2' => [],
            ],
            'dept_persons' => [],
            'job_persons' => [
                'M1-D-0-L-1-J-M1' => [],
                'M2-D-1-L-1-J-M1' => [],
                'M3-D-1-L-1-J-M2' => [],
                'M3-D-2-L-1-J-M2' => [],
                'M4-D-1-L-1-J-M2' => [],
                'M5-D-2-L-1-J-M2' => [],
                'PRES-D-0-L-1-J-P1' => [],
                'S1-D-1-L-1-J-S1' => [],
                'S2-D-2-L-1-J-S1' => [],
                'VP1-D-1-L-1-J-VP1' => [],
                'VP2-D-2-L-1-J-VP2' => [],
            ],
        ];
        $counts = [
            'phones' => 11,
            'emails' => 11,
        ];
        foreach ($expected as $key => $ids) {
            $this->assertArrayHasKey($key, $r);
            foreach ($ids as $id => $empty) {
                $this->assertArrayHasKey($id, $r[$key]);
            }
        }
        foreach ($counts as $key => $count) {
            $this->assertArrayHasKey($key, $r);
            $this->assertCount($count, $r[$key]);
        }
    }

    public function testWithEmpty()
    {
        $sut = $this->getSut();
        $import = $this->makeImport();
        $r = $sut->run($import)->getCounters();
        $expected = [
            'person::total' => 0,
            'location::total' => 0,
            'department::total' => 0,
            'job::total' => 0,
            'job_person::total' => 0,
            'type::total' => 0,
            'email::total' => 0,
            'phone::total' => 0,
            'person::updated_or_created' => 0,
            'location::updated_or_created' => 0,
            'department::updated_or_created' => 0,
            'job::updated_or_created' => 0,
            'job_person::updated_or_created' => 0,
            'type::updated_or_created' => 0,
            'email::updated_or_created' => 0,
            'phone::updated_or_created' => 0,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r);
    }

    public function testWithFakeCompany()
    {
        $sut = $this->getSut();
        $import = $this->makeImport();
        $this->createFakeCompany();
        $r = $sut->run($import)->getCounters();
        $expected = [
            'person::total' => 10,
            'location::total' => 1,
            'department::total' => 3,
            'job::total' => 6,
            'job_person::total' => 11,
            'type::total' => 3,
            'email::total' => 11,
            'phone::total' => 11,
            'person::updated_or_created' => 10,
            'location::updated_or_created' => 1,
            'department::updated_or_created' => 3,
            'job::updated_or_created' => 6,
            'job_person::updated_or_created' => 11,
            'type::updated_or_created' => 3,
            'email::updated_or_created' => 11,
            'phone::updated_or_created' => 11,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r);
        $this->verifyJobPerson('PRES', 1, [
            [
                'person_id' => 'PRES',
                'department_id' => 'D-0',
                'job_id' => 'J-P1',
                'supervisor_id' => '0',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP1', 1, [
            [
                'person_id' => 'VP1',
                'department_id' => 'D-1',
                'job_id' => 'J-VP1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S1', 1, [
            [
                'person_id' => 'S1',
                'department_id' => 'D-1',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP2', 1, [
            [
                'person_id' => 'VP2',
                'department_id' => 'D-2',
                'job_id' => 'J-VP2',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S2', 1, [
            [
                'person_id' => 'S2',
                'department_id' => 'D-2',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP2',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M1', 1, [
            [
                'person_id' => 'M1',
                'department_id' => 'D-0',
                'job_id' => 'J-M1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M2', 1, [
            [
                'person_id' => 'M2',
                'department_id' => 'D-1',
                'job_id' => 'J-M1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M3', 2, [
            [
                'person_id' => 'M3',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
            [
                'person_id' => 'M3',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M4', 1, [
            [
                'person_id' => 'M4',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M5', 1, [
            [
                'person_id' => 'M5',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
    }

    public function testViaTempWithFakeCompanyWithUpdates()
    {
        $sut = $this->getSut();
        $import = $this->makeImportViaTemp();
        $this->createFakeCompany();
        $r = $sut->run($import)->getCounters();
        HR::where('department_id', '=', 'D-1')->update(['department' => 'Department One']);
        HR::where('job_id', '=', 'J-M1')->update(['job_desc' => 'Minion One']);
        $r = $sut->run($import)->getCounters();
        $expected = [
            'persons::total' => 10,
            'locations::total' => 1,
            'depts::total' => 3,
            'jobs::total' => 6,
            'job_persons::total' => 11,
            'types::total' => 3,
            'emails::total' => 11,
            'phones::total' => 11,
            'persons::created' => 0,
            'persons::updated' => 10,
            'locations::created' => 0,
            'locations::updated' => 1,
            'departments::created' => 0,
            'departments::updated' => 3,
            'jobs::created' => 0,
            'jobs::updated' => 6,
            'job_persons::created' => 0,
            'job_persons::updated' => 11,
            'types::created' => 0,
            'types::updated' => 3,
            'emails::created' => 0,
            'emails::updated' => 11,
            'phones::created' => 0,
            'phones::updated' => 11,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r);
        $d = Department::where('primary_id', '=', 'D-1')->first();
        $this->assertEquals('Department One', $d->name);
        $j = Job::where('primary_id', '=', 'J-M1')->first();
        $this->assertEquals('Minion One', $j->name);
        $this->verifyJobPerson('PRES', 1, [
            [
                'person_id' => 'PRES',
                'department_id' => 'D-0',
                'job_id' => 'J-P1',
                'supervisor_id' => '0',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP1', 1, [
            [
                'person_id' => 'VP1',
                'department_id' => 'D-1',
                'job_id' => 'J-VP1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S1', 1, [
            [
                'person_id' => 'S1',
                'department_id' => 'D-1',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP2', 1, [
            [
                'person_id' => 'VP2',
                'department_id' => 'D-2',
                'job_id' => 'J-VP2',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S2', 1, [
            [
                'person_id' => 'S2',
                'department_id' => 'D-2',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP2',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M1', 1, [
            [
                'person_id' => 'M1',
                'department_id' => 'D-0',
                'job_id' => 'J-M1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M2', 1, [
            [
                'person_id' => 'M2',
                'department_id' => 'D-1',
                'job_id' => 'J-M1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M3', 2, [
            [
                'person_id' => 'M3',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
            [
                'person_id' => 'M3',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M4', 1, [
            [
                'person_id' => 'M4',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M5', 1, [
            [
                'person_id' => 'M5',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
    }

    public function testViaTempWithFakeCompany()
    {
        $sut = $this->getSut();
        $import = $this->makeImportViaTemp();
        $this->createFakeCompany();
        $r = $sut->run($import)->getCounters();
        $expected = [
            'persons::total' => 10,
            'locations::total' => 1,
            'depts::total' => 3,
            'jobs::total' => 6,
            'job_persons::total' => 11,
            'types::total' => 3,
            'emails::total' => 11,
            'phones::total' => 11,
            'persons::created' => 10,
            'persons::updated' => 0,
            'locations::created' => 1,
            'locations::updated' => 0,
            'departments::created' => 3,
            'departments::updated' => 0,
            'jobs::created' => 6,
            'jobs::updated' => 0,
            'job_persons::created' => 11,
            'job_persons::updated' => 0,
            'types::created' => 3,
            'types::updated' => 0,
            'emails::created' => 11,
            'emails::updated' => 0,
            'phones::created' => 11,
            'phones::updated' => 0,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r);
        $this->verifyJobPerson('PRES', 1, [
            [
                'person_id' => 'PRES',
                'department_id' => 'D-0',
                'job_id' => 'J-P1',
                'supervisor_id' => '0',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP1', 1, [
            [
                'person_id' => 'VP1',
                'department_id' => 'D-1',
                'job_id' => 'J-VP1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S1', 1, [
            [
                'person_id' => 'S1',
                'department_id' => 'D-1',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('VP2', 1, [
            [
                'person_id' => 'VP2',
                'department_id' => 'D-2',
                'job_id' => 'J-VP2',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('S2', 1, [
            [
                'person_id' => 'S2',
                'department_id' => 'D-2',
                'job_id' => 'J-S1',
                'supervisor_id' => 'VP2',
                'location_id' => 'L-1',
                'manages' => '1',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M1', 1, [
            [
                'person_id' => 'M1',
                'department_id' => 'D-0',
                'job_id' => 'J-M1',
                'supervisor_id' => 'PRES',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M2', 1, [
            [
                'person_id' => 'M2',
                'department_id' => 'D-1',
                'job_id' => 'J-M1',
                'supervisor_id' => 'VP1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M3', 2, [
            [
                'person_id' => 'M3',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
            [
                'person_id' => 'M3',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M4', 1, [
            [
                'person_id' => 'M4',
                'department_id' => 'D-1',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S1',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
        $this->verifyJobPerson('M5', 1, [
            [
                'person_id' => 'M5',
                'department_id' => 'D-2',
                'job_id' => 'J-M2',
                'supervisor_id' => 'S2',
                'location_id' => 'L-1',
                'manages' => '0',
                'deleted_at' => null,
            ],
        ]);
    }

    public function testWithOneRecord()
    {
        $sut = $this->getSut();
        $this->createPeople();
        $import = $this->makeImport();
        $r = $sut->run($import)->getCounters();
        $expected = [
            'person::total' => 1,
            'location::total' => 1,
            'department::total' => 1,
            'job::total' => 1,
            'job_person::total' => 1,
            'type::total' => 1,
            'email::total' => 1,
            'phone::total' => 1,
            'person::updated_or_created' => 1,
            'location::updated_or_created' => 1,
            'department::updated_or_created' => 1,
            'job::updated_or_created' => 1,
            'job_person::updated_or_created' => 1,
            'type::updated_or_created' => 1,
            'email::updated_or_created' => 1,
            'phone::updated_or_created' => 1,
            'errors' => 0,
        ];
        $this->assertEquals($expected, $r);
    }

    protected function compareJobPerson($expected, $model)
    {
        $attrs = $model->getAttributes();
        unset($attrs['id']);
        unset($attrs['created_at']);
        unset($attrs['updated_at']);
        $this->assertEquals($expected, $attrs);
    }

    /**
     * @return Runner
     */
    protected function getSut()
    {
        return $this->app[Runner::class];
    }

    protected function makeImport()
    {
        $import = new Import();
        $importProvider = new ImportProvider();
        $importProvider->id = 'hr';
        $importProvider->name = 'Testing';
        $importProvider->provider = \Smorken\Importer\Import\Providers\HR\Import::class;
        $import->forceFill(['id' => 1, 'provider_id' => 'hr']);
        //$import->importProvider = $importProvider;
        return $import;
    }

    protected function makeImportViaTemp()
    {
        $import = new Import();
        $importProvider = new ImportProvider();
        $importProvider->id = 'hrt';
        $importProvider->name = 'Testing';
        $importProvider->provider = \Smorken\Importer\Import\Providers\HR\ImportViaTemp::class;
        $import->forceFill(['id' => 2, 'provider_id' => 'hrt']);
        //$import->importProvider = $importProvider;
        return $import;
    }

    protected function verifyJobPerson($id, $count, $expected)
    {
        $p = Person::where('primary_id', '=', $id)->first();
        $jobs = $p->jobPivots;
        $this->assertCount($count, $jobs);
        foreach ($jobs as $i => $job) {
            if (isset($expected[$i])) {
                $this->compareJobPerson($expected[$i], $job);
            } else {
                throw new \OutOfBoundsException("Expected array does not contain job [$i].");
            }
        }
    }
}
