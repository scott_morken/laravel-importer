<?php


namespace Smorken\Importer\Tests\Feature\Cleanup\HR;


use Smorken\Importer\Contracts\Import\CleanupHandler;
use Smorken\Importer\Models\Eloquent\HR\Department;
use Smorken\Importer\Models\Eloquent\HR\Job;
use Smorken\Importer\Models\Eloquent\HR\JobPerson;
use Smorken\Importer\Models\Eloquent\HR\Location;
use Smorken\Importer\Models\Eloquent\HR\Person;
use Smorken\Importer\Models\Eloquent\HR\Type;
use Smorken\Importer\Tests\TestCase;

class CleanupHandlerTest extends TestCase
{

    /**
     * @return CleanupHandler
     */
    public function getSut()
    {
        return $this->app[CleanupHandler::class];
    }

    public function testAllWithOrphans()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-18 months'));
        $type->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $type->save();
        $sut = $this->getSut();
        //base
        $r = $sut->run('cleanup.base');//creates orphaned job_person records
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,//orphans check will catch this case
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
        //permanent
        $type->forceFill(['updated_at' => $deldate]);
        $type->save();
        $r = $sut->run('cleanup.permanent', true);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
            ],
            'delete' => [
            ],
        ];
        $this->assertEquals($expected, $r[0]);
        //orphans
        $r = $sut->run('cleanup.orphans', true);
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
            ],
            'delete' => [
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseDepartmentPastBaseForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $dept2 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept2->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $dept1->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $dept1->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base', true);
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
            ],
            'delete' => [
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseDepartmentPastBaseNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $dept2 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept2->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $dept1->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $dept1->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseDepartmentPastPermanentNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $dept2 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept2->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-18 months'));
        $dept1->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $dept1->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseJobPastBaseNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $job1->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $job1->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseJobPastBaseWithModifyQueryNotForcedLimitsToModifiedResults()
    {
        $person = factory(Person::class)->create();
        $type1 = factory(Type::class)->create();
        $type2 = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type1->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type2->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $job1->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $job1->save();
        $job2->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $job2->save();
        $sut = $this->getSut();
        $sut->setAttribute('base', 'type_id', $type2->id);
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseLocationPastBaseNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $loc->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $loc->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 2, //direct relation, not orphaned
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 1,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseRecentDataNotForcedNotCleaned()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept->id, 'location_id' => $loc->id,
        ]);
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testBaseTypePastBaseNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $type->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $type->save();
        $sut = $this->getSut();
        $r = $sut->run('cleanup.base');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
            'forceDelete' => [

            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\Person' => 0,
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 0,//orphans check will catch this case
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Type' => 1,
                'Smorken\Importer\Contracts\Storage\HR\Job' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Department' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Location' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }

    public function testOrphanedJobPersonNotForced()
    {
        $person = factory(Person::class)->create();
        $type = factory(Type::class)->create();
        $job1 = factory(Job::class)->create(['type_id' => $type->id]);
        $job2 = factory(Job::class)->create(['type_id' => $type->id]);
        $dept1 = factory(Department::class)->create();
        $loc = factory(Location::class)->create();
        $jp1 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job1->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $jp2 = factory(JobPerson::class)->create([
            'person_id' => $person->id, 'job_id' => $job2->id, 'department_id' => $dept1->id, 'location_id' => $loc->id,
        ]);
        $deldate = date('Y-m-d H:i:s', strtotime('-1 month'));
        $type->forceFill(['created_at' => $deldate, 'updated_at' => $deldate]);
        $type->save();
        $sut = $this->getSut();
        $sut->run('cleanup.base');//creates orphaned job_person records
        $r = $sut->run('cleanup.orphans');
        $this->assertCount(1, $r);
        $expected = [
            'stale' => [
            ],
            'forceDelete' => [
            ],
            'delete' => [
                'Smorken\Importer\Contracts\Storage\HR\JobPerson' => 2,
                'Smorken\Importer\Contracts\Storage\HR\Email' => 0,
                'Smorken\Importer\Contracts\Storage\HR\Phone' => 0,
            ],
        ];
        $this->assertEquals($expected, $r[0]);
    }
}
