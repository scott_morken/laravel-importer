<?php


namespace Smorken\Importer\Tests\Feature\Stubs\Storage;


use Closure;
use Smorken\Storage\Abstraction\Eloquent;

class HR extends Eloquent implements \Smorken\Importer\Tests\Feature\Stubs\Contracts\Storage\HR
{

    public function getActiveEmployees(Closure $callback, $chunk_size = 200)
    {
        return $this->getModel()->newQuery()->chunk($chunk_size, $callback);
    }
}
