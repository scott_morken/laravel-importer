<?php


namespace Smorken\Importer\Tests\Feature\Stubs\Contracts\Storage;


use Closure;

interface HR
{

    public function getActiveEmployees(Closure $callback, $chunk_size = 200);
}
