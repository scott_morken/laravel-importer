<?php


namespace Smorken\Importer\Tests\Feature\Stubs\Models;


use Smorken\Model\Eloquent;

class HR extends Eloquent implements \Smorken\Importer\Tests\Feature\Stubs\Contracts\Models\HR
{

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'id',
        'department_id',
        'department',
        'email',
        'job_id',
        'job_desc',
        'type_id',
        'job_type',
        'location_id',
        'location',
        'supervisor_id',
        'manages_department_id',
        'first_name',
        'last_name',
        'phone',
    ];

    protected $table = 'hr';
}
