<?php


namespace Smorken\Importer\Tests\Traits;


use Smorken\Importer\Tests\Feature\Stubs\Models\HR;

trait Data
{

    protected function createFakeCompany()
    {
        $locations = [
            'L-1' => [
                'location_id' => 'L-1',
                'location' => 'Location 1',
            ],
        ];
        $depts = [
            'D-0' => [
                'department_id' => 'D-0',
                'department' => 'Dept of President',
            ],
            'D-1' => [
                'department_id' => 'D-1',
                'department' => 'Department 1',
            ],
            'D-2' => [
                'department_id' => 'D-2',
                'department' => 'Department 2',
            ],
        ];
        $jobs = [
            'J-P1' => [
                'job_id' => 'J-P1',
                'type_id' => 'T-1',
                'job_desc' => 'President',
                'job_type' => 'C',
            ],
            'J-VP1' => [
                'job_id' => 'J-VP1',
                'type_id' => 'T-1',
                'job_desc' => 'VP 1',
                'job_type' => 'C',
            ],
            'J-VP2' => [
                'job_id' => 'J-VP2',
                'type_id' => 'T-1',
                'job_desc' => 'VP 2',
                'job_type' => 'C',
            ],
            'J-S1' => [
                'job_id' => 'J-S1',
                'type_id' => 'T-2',
                'job_desc' => 'Supervisor 1',
                'job_type' => 'S',
            ],
            'J-M1' => [
                'job_id' => 'J-M1',
                'type_id' => 'T-3',
                'job_desc' => 'Minion 1',
                'job_type' => 'M',
            ],
            'J-M2' => [
                'job_id' => 'J-M2',
                'type_id' => 'T-3',
                'job_desc' => 'Minion 2',
                'job_type' => 'M',
            ],
        ];
        $pres = $this->createPerson($locations['L-1'], $depts['D-0'], $jobs['J-P1'],
            ['supervisor_id' => 0, 'manages_department_id' => 'D-0', 'id' => 'PRES']);
        $vp1 = $this->createPerson($locations['L-1'], $depts['D-1'], $jobs['J-VP1'],
            ['supervisor_id' => $pres->id, 'manages_department_id' => 'D-1', 'id' => 'VP1']);
        $vp2 = $this->createPerson($locations['L-1'], $depts['D-2'], $jobs['J-VP2'],
            ['supervisor_id' => $pres->id, 'manages_department_id' => 'D-2', 'id' => 'VP2']);
        $super1 = $this->createPerson($locations['L-1'], $depts['D-1'], $jobs['J-S1'],
            ['supervisor_id' => $vp1->id, 'manages_department_id' => 'D-1', 'id' => 'S1']);
        $super2 = $this->createPerson($locations['L-1'], $depts['D-2'], $jobs['J-S1'],
            ['supervisor_id' => $vp2->id, 'manages_department_id' => 'D-2', 'id' => 'S2']);
        $minions = [
            $this->createPerson($locations['L-1'], $depts['D-0'], $jobs['J-M1'],
                ['supervisor_id' => $pres->id, 'manages_department_id' => 0, 'id' => 'M1']),
            $this->createPerson($locations['L-1'], $depts['D-1'], $jobs['J-M1'],
                ['supervisor_id' => $vp1->id, 'manages_department_id' => 0, 'id' => 'M2']),
            $this->createPerson($locations['L-1'], $depts['D-1'], $jobs['J-M2'],
                ['supervisor_id' => $super1->id, 'manages_department_id' => 0, 'id' => 'M3']),
            $this->createPerson($locations['L-1'], $depts['D-1'], $jobs['J-M2'],
                ['supervisor_id' => $super1->id, 'manages_department_id' => 0, 'id' => 'M4']),
            $this->createPerson($locations['L-1'], $depts['D-2'], $jobs['J-M2'],
                ['supervisor_id' => $super2->id, 'manages_department_id' => 0, 'id' => 'M5']),
            $this->createPerson($locations['L-1'], $depts['D-2'], $jobs['J-M2'],
                ['supervisor_id' => $super2->id, 'manages_department_id' => 0, 'id' => 'M3']),
        ];
    }

    protected function createPeople($count = 1)
    {
        return factory(HR::class, $count)->create();
    }

    protected function createPerson($location, $dept, $job, $add_attrs = [])
    {
        $attrs = array_replace($location, $dept, $job, $add_attrs);
        return factory(HR::class)->create($attrs);
    }
}
