<?php


namespace Smorken\Importer\Tests\Traits;


trait Trim
{

    protected function trim($string)
    {
        return preg_replace('/\s+/', ' ', preg_replace('/\r\n|\r|\n/', '', $string));
    }
}
