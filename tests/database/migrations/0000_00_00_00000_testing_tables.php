<?php

use Illuminate\Database\Migrations\Migration;
use Smorken\Importer\Tests\Traits\Table;

class TestingTables extends Migration
{

    use Table;

    protected $connection = null;

    public function down()
    {
        $this->dropTables();
    }

    public function up()
    {
        $this->createTables();
    }

    protected function createTables()
    {
        $attrs = [
            'id',
            'department_id',
            'department',
            'email',
            'job_id',
            'job_desc',
            'type_id',
            'job_type',
            'location_id',
            'location',
            'supervisor_id',
            'manages_department_id',
            'first_name',
            'last_name',
            'phone',
        ];
        $this->createTableFromAttributes('HR', $attrs, [], $this->connection);
    }

    protected function dropTables()
    {
        $tables = [
            'HR',
        ];
        foreach ($tables as $t) {
            $this->deleteTable($t, $this->connection);
        }
    }
}
