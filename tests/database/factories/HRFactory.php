<?php

use Smorken\Importer\Tests\Feature\Stubs\Models\HR;

$factory->define(
    HR::class,
    function (\Faker\Generator $faker) {
        return [
            'id' => 'P-'.$faker->randomNumber(5),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'department_id' => 'D-'.$faker->randomNumber(1),
            'department' => 'DEPT: '.$faker->words(2, true),
            'type_id' => 'T-'.$faker->randomNumber(1),
            'job_type' => 'TYPE: '.$faker->words(2, true),
            'job_id' => 'J-'.$faker->randomNumber(2),
            'job_desc' => 'JOB: '.$faker->jobTitle,
            'manages_department_id' => $faker->randomElement(['D-'.$faker->randomNumber(1), null]),
            'supervisor_id' => 'P-'.$faker->randomNumber(5),
            'email' => $faker->email,
            'phone' => $faker->phoneNumber,
            'location_id' => 'L-1',
            'location' => 'LOC: One',
        ];
    }
);
