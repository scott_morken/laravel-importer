<?php namespace Smorken\Importer\Tests;

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Gate;
use Mockery as m;
use Smorken\Importer\CleanupServiceProvider;
use Smorken\Importer\Import\Providers\HR\Import;
use Smorken\Importer\Import\Providers\HR\ImportViaTemp;
use Smorken\Importer\Models\Eloquent\HR\Department;
use Smorken\Importer\Models\Eloquent\HR\Email;
use Smorken\Importer\Models\Eloquent\HR\Job;
use Smorken\Importer\Models\Eloquent\HR\JobPerson;
use Smorken\Importer\Models\Eloquent\HR\Location;
use Smorken\Importer\Models\Eloquent\HR\Person;
use Smorken\Importer\Models\Eloquent\HR\Phone;
use Smorken\Importer\Models\Eloquent\HR\Type;
use Smorken\Importer\Tests\Feature\Stubs\Contracts\Storage\HR;
use Smorken\Sanitizer\ServiceProvider;
use Smorken\Support\Contracts\Binder;

abstract class TestCase extends \Orchestra\Testbench\BrowserKit\TestCase
{

    use DatabaseMigrations;

    protected $admins = [];

    protected $baseUrl = 'http://localhost';

    public function setUp(): void
    {
        parent::setUp();
        $this->seedApp();
        $this->defineGates($this->app);
    }

    protected function addFactories($app, $paths = [])
    {
        $f = $app->make('Illuminate\Database\Eloquent\Factory');
        foreach ($paths as $path) {
            $path = realpath($path);
            if ($path) {
                $f->load($path);
            }
        }
    }

    protected function addMigrations($app, $paths = [])
    {
        $reals = array_map(
            function ($v) {
                return realpath($v);
            },
            $paths
        );
        if ($reals) {
            $app->afterResolving(
                'migrator',
                function ($migrator) use ($reals) {
                    foreach ($reals as $real) {
                        $migrator->path($real);
                    }
                }
            );
        }
    }

    protected function bindHRProviders(Binder $binder)
    {
        $binder->bindAll(
            [
                'contract' => [
                    \Smorken\Importer\Contracts\Storage\HR\Department::class => \Smorken\Importer\Storage\Eloquent\HR\Department::class,
                    \Smorken\Importer\Contracts\Storage\HR\Email::class => \Smorken\Importer\Storage\Eloquent\HR\Email::class,
                    \Smorken\Importer\Contracts\Storage\HR\Job::class => \Smorken\Importer\Storage\Eloquent\HR\Job::class,
                    \Smorken\Importer\Contracts\Storage\HR\JobPerson::class => \Smorken\Importer\Storage\Eloquent\HR\JobPerson::class,
                    \Smorken\Importer\Contracts\Storage\HR\Location::class => \Smorken\Importer\Storage\Eloquent\HR\Location::class,
                    \Smorken\Importer\Contracts\Storage\HR\Person::class => \Smorken\Importer\Storage\Eloquent\HR\Person::class,
                    \Smorken\Importer\Contracts\Storage\HR\Phone::class => \Smorken\Importer\Storage\Eloquent\HR\Phone::class,
                    \Smorken\Importer\Contracts\Storage\HR\Type::class => \Smorken\Importer\Storage\Eloquent\HR\Type::class,
                    HR::class => Feature\Stubs\Storage\HR::class,
                ],
                'concrete' => [
                    \Smorken\Importer\Storage\Eloquent\HR\Department::class => [
                        'model' => [
                            'impl' => Department::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Email::class => [
                        'model' => [
                            'impl' => Email::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Job::class => [
                        'model' => [
                            'impl' => Job::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\JobPerson::class => [
                        'model' => [
                            'impl' => JobPerson::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Location::class => [
                        'model' => [
                            'impl' => Location::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Person::class => [
                        'model' => [
                            'impl' => Person::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Phone::class => [
                        'model' => [
                            'impl' => Phone::class,
                        ],
                    ],
                    \Smorken\Importer\Storage\Eloquent\HR\Type::class => [
                        'model' => [
                            'impl' => Type::class,
                        ],
                    ],
                    Feature\Stubs\Storage\HR::class => [
                        'model' => [
                            'impl' => Feature\Stubs\Models\HR::class,
                        ],
                    ],
                ],
            ]
        );
    }

    protected function defineGates($app)
    {
        Gate::define(
            'role-admin',
            function ($user) {
                return in_array((int) $user->id, $this->admins, true);
            }
        );
    }

    /**
     * Define environment setup.
     *
     * @param  Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set(
            'database.connections.testbench',
            [
                'driver' => 'sqlite',
                'database' => '/app/tests/database/testing.sqlite',
                'prefix' => '',
            ]
        );
        $app['config']->set('importer.view_master', 'smorken/importer::layouts.master');
        $app['config']->set('importer.hr_data', true);
        $app['config']->set(
            'importer_providers.cleanup',
            [
                'base' => [
                    [
                        'id' => 'hr',
                        'provider' => \Smorken\Importer\Import\Cleanup\HR\Handler::class,
                        'params' => [],
                    ],
                ],
                'permanent' => [
                    [
                        'id' => 'hr',
                        'provider' => \Smorken\Importer\Import\Cleanup\HR\Handler::class,
                        'params' => [],
                    ],
                ],
                'orphans' => [
                    [
                        'id' => 'hr',
                        'provider' => \Smorken\Importer\Import\Cleanup\HR\Orphans::class,
                        'params' => [],
                    ],
                ],
            ]
        );
        $app['config']->set(
            'importer_providers.importers',
            [
                [
                    'id' => 'hr',
                    'name' => 'HR Data',
                    'provider' => Import::class,
                    'params' => ['hrProvider' => HR::class],
                ],
                [
                    'id' => 'hrt',
                    'name' => 'HR Data via Temp',
                    'provider' => ImportViaTemp::class,
                    'params' => ['hrProvider' => HR::class],
                ],
            ]
        );
        $this->addMigrations($app, [__DIR__.'/database/migrations', '/app/hr/database/migrations']);
        $this->addFactories($app, [__DIR__.'/database/factories', '/app/hr/database/factories']);
        $this->bindHRProviders($app[Binder::class]);
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Importer\ServiceProvider::class,
            CleanupServiceProvider::class,
            \Smorken\Ext\Controller\ServiceProvider::class,
            \Smorken\Support\ServiceProvider::class,
            \Smorken\Ext\Database\ServiceProvider::class,
            ServiceProvider::class,
            \Smorken\CacheAssist\ServiceProvider::class,
        ];
    }

    protected function mockAuth($user_data = ['id' => 1, 'first_name' => 'foo', 'last_name' => 'bar'], $admin = true)
    {
        $user = m::mock('Illuminate\Contracts\Auth\Authenticatable');
        foreach ($user_data as $k => $v) {
            $user->$k = $v;
        }
        if ($admin) {
            $this->admins[] = (int) $user->id;
        }
        $user->shouldReceive('getAuthIdentifier')->andReturn($user->id);
        $this->be($user);
        return $user;
    }

    protected function seedApp()
    {
    }
}
