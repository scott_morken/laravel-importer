<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 9:59 AM
 */
return [
    'from' => env('IMPORTER_FROM', 'importer@app.example.edu'),
    'load_routes' => env('IMPORTER_LOAD_ROUTES', true),
    'middleware' => ['web', 'auth', 'can:role-admin'],
    'cleanup' => [
        'base' => env('IMPORTER_CLEANUP_BASE', '-14 days'),
        'permanent' => env('IMPORTER_CLEANUP_PERMANENT', '-1 year'),
        'orphans' => env('IMPORTER_CLEANUP_ORPHANS', '-14 days'),
    ],
    'storage' => [
        'contract' => [
            \Smorken\Importer\Contracts\Storage\Import::class => \Smorken\Importer\Storage\Eloquent\Import::class,
            \Smorken\Importer\Contracts\Storage\ImportProvider::class => \Smorken\Importer\Storage\Config\ImportProvider::class,
            \Smorken\Importer\Contracts\Storage\ImportRun::class => \Smorken\Importer\Storage\Eloquent\ImportRun::class,
            //HR
//            \Smorken\Importer\Contracts\Storage\HR\Department::class => \Smorken\Importer\Storage\Eloquent\HR\Department::class,
//            \Smorken\Importer\Contracts\Storage\HR\Email::class => \Smorken\Importer\Storage\Eloquent\HR\Email::class,
//            \Smorken\Importer\Contracts\Storage\HR\Job::class => \Smorken\Importer\Storage\Eloquent\HR\Job::class,
//            \Smorken\Importer\Contracts\Storage\HR\JobPerson::class => \Smorken\Importer\Storage\Eloquent\HR\JobPerson::class,
//            \Smorken\Importer\Contracts\Storage\HR\Location::class => \Smorken\Importer\Storage\Eloquent\HR\Location::class,
//            \Smorken\Importer\Contracts\Storage\HR\Person::class => \Smorken\Importer\Storage\Eloquent\HR\Person::class,
//            \Smorken\Importer\Contracts\Storage\HR\Phone::class => \Smorken\Importer\Storage\Eloquent\HR\Phone::class,
//            \Smorken\Importer\Contracts\Storage\HR\Type::class => \Smorken\Importer\Storage\Eloquent\HR\Type::class,
        ],
        'concrete' => [
            \Smorken\Importer\Storage\Eloquent\Import::class => [
                'model' => [
                    'impl' => \Smorken\Importer\Models\Eloquent\Import::class,
                ],
            ],
            \Smorken\Importer\Storage\Config\ImportProvider::class => [
                'model' => [
                    'impl' => \Smorken\Importer\Models\Config\ImportProvider::class,
                ],
            ],
            \Smorken\Importer\Storage\Eloquent\ImportRun::class => [
                'model' => [
                    'impl' => \Smorken\Importer\Models\Eloquent\ImportRun::class,
                ],
                'vo' => [
                    'impl' => \Smorken\Importer\Models\VO\ImportResult::class,
                ],
            ],
            //HR
//            \Smorken\Importer\Storage\Eloquent\HR\Department::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Department::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Email::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Email::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Job::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Job::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\JobPerson::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\JobPerson::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Location::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Location::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Person::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Person::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Phone::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Phone::class,
//                ],
//            ],
//            \Smorken\Importer\Storage\Eloquent\HR\Type::class => [
//                'model' => [
//                    'impl' => \Smorken\Importer\Models\Eloquent\HR\Type::class,
//                ],
//            ],
        ],
    ],
];
