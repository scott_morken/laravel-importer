<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style></style>
</head>
<body>
<h4>Import run results for {{ $import->id }}: {{ $import->descr }}</h4>
<div>Provider: {{ $import->importProvider->name }}</div>
<div>Run at: {{ $import_run->created_at->toDayDateTimeString() }}</div>
<div>Run by: {{ $import_run->run_by_id }}</div>
<hr/>
<div>
    <?php
    $results = $import_run->results ?: [];
    $results = $results instanceof \Smorken\Importer\Contracts\Models\ImportResult ? $results->toArray() : $results;
    $messages = $results['messages'] ?? [];
    unset($results['messages']);
    ?>
    <table>
        <thead>
        <tr>
            <th>Key</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        @foreach($results as $k => $count)
            <tr>
                <td>{{ $k }}</td>
                <td>{{ $count }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="">Messages</div>
    <pre>{{ \Smorken\Support\Arr::stringify($messages) }}</pre>
</div>
</body>
</html>
