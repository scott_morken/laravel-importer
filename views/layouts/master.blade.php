<!DOCTYPE html>
<html>
<head>

</head>
<body>
<div class="container">
    @includeIf('smorken/importer::layouts._partials._flash')
    @if (isset($errors) && $errors->any())
        <div class="alert alert-danger mt-2 mb-2">
            <ul class="container">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div id="content">
        @yield('content')
    </div>
</div>
@stack('add_to_end')
</body>
</html>
