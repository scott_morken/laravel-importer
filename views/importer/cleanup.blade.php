@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@section('content')
    @include('smorken/importer::_preset.controller._to_index')
    @include('smorken/importer::_preset.controller._title', ['title' => 'Importer Administration'])
    <h4>Cleanup</h4>
    @foreach($results as $k => $k_results)
        <h4>{{ $k }}</h4>
        <dl>
            @foreach($k_results as $item => $contracts)
                <dt>{{ $item }}</dt>
                <dd>
                    @forelse($contracts as $c => $count)
                        <div>{{ $c }}: {{ $count }}</div>
                    @empty
                        <div>None</div>
                    @endforelse
                </dd>
            @endforeach
        </dl>
    @endforeach
@endsection
