@include('smorken/importer::_preset.input.g_input', ['name' => 'descr', 'title' => 'Description'])
@include('smorken/importer::_preset.input.g_input', ['name' => 'email_to', 'title' => 'Email To'])
@include('smorken/importer::_preset.input.g_select', ['name' => 'provider_id', 'title' => 'Provider', 'items' => $import_providers->pluck('name', 'id')])
@include('smorken/importer::_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
