@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@include('smorken/importer::_preset.controller.view', [
    'title' => 'Importer Administration',
    'extra' => ['Provider' => $model->importProvider ? $model->importProvider->name : $model->provider_id]
])
@section('content')
    <div class="card">
        <div class="card-header">Last five import run results</div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Run by</th>
                <th>Run at</th>
            </tr>
            </thead>
            <tbody>
            @if (count($imports))
                @foreach($imports as $run)
                    <tr>
                        <td>
                            @include('smorken/importer::_preset.input._anchor', [
                            'href' => action([$controller, 'runView'], ['import_id' => $model->id, 'run_id' => $run->id]),
                            'title' => $run->id
                            ])
                        </td>
                        <td>{{ $run->run_by_id }}</td>
                        <td>{{ $run->created_at->toDayDateTimeString() }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="3">No import run results.</td>
                </tr>
            @endif
            </tbody>
        </table>
        <div class="card-footer">
            @include('smorken/importer::_preset.input._anchor', [
            'href' => action([$controller, 'run'], ['id' => $model->id]),
            'title' => 'Import now',
            'classes' => 'btn btn-success mr-2',
            ])
            @include('smorken/importer::_preset.input._anchor', [
            'href' => action([$controller, 'sample'], ['id' => $model->id]),
            'title' => 'Sample data',
            'classes' => 'btn btn-outline-primary',
            ])
        </div>
    </div>
@append
