@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@section('content')
    @include('smorken/importer::_preset.controller._to_index')
    @include('smorken/importer::_preset.controller._title', ['title' => 'Importer Administration'])
    <h4>Sample run results for {{ $import->id }}: {{ $import->descr }}</h4>
    <div class="mb-2">
        @include('smorken/importer::_preset.input._anchor', [
            'href' => action([$controller, 'view'], ['id' => $import->id]),
            'title' => 'Back to view',
        ])
    </div>
    <div>Provider: {{ $import->importProvider ? $import->importProvider->name : $import->provider_id }}</div>
    <div class="card">
        <div class="card-body">
            <pre>{{ \Smorken\Support\Arr::stringify($results) }}</pre>
        </div>
    </div>
@endsection
