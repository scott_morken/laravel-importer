@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@section('content')
    @include('smorken/importer::_preset.controller._to_index')
    @include('smorken/importer::_preset.controller._title', ['title' => 'Importer Administration'])
    <h4>Import run results for {{ $import->id }}: {{ $import->descr }}</h4>
    <div class="mb-2">
        @include('smorken/importer::_preset.input._anchor', [
            'href' => action([$controller, 'view'], ['id' => $import->id]),
            'title' => 'Back to view',
        ])
    </div>
    <div>Provider: {{ $import->importProvider ? $import->importProvider->name : $import->provider_id }}</div>
    <div>Run at: {{ $run->created_at->toDayDateTimeString() }}</div>
    <div>Run by: {{ $run->run_by_id }}</div>
    <div class="card">
        <div class="card-body">
            <?php
            $results = $run->results ?: [];
            $results = $results instanceof \Smorken\Importer\Contracts\Models\ImportResult ? $results->toArray() : $results;
            $messages = $results['messages'] ?? [];
            unset($results['messages']);
            ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Key</th>
                    <th>Value</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $k => $count)
                    <tr>
                        <td>{{ $k }}</td>
                        <td>{{ $count }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="font-weight-bold">Messages</div>
            <pre>{{ \Smorken\Support\Arr::stringify($messages) }}</pre>
        </div>
    </div>
@endsection
