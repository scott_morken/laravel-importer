@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@section('content')
    @include('smorken/importer::_preset.controller._title', ['title' => 'Importer Administration'])
    @include('smorken/importer::_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Provider</th>
                <th>Email To</th>
                <th>Active</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>
                        @include('smorken/importer::_preset.controller.index_actions._view', ['value' => $model->getKey()])
                    </td>
                    <td>{{ $model->descr }}</td>
                    <td>{{ $model->importProvider ? $model->importProvider->name : $model->provider_id }}</td>
                    <td>{{ $model->email_to }}</td>
                    <td>{{ $model->active ? 'Yes' : 'No' }}</td>
                    <td>
                        @include('smorken/importer::_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends(isset($filter)?$filter->except(['page']):[])->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
