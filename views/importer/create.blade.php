@extends(\Illuminate\Support\Facades\Config::get('importer.view_master', 'layouts.app'))
@include('smorken/importer::_preset.controller.create', ['title' => 'Importer Administration', 'inputs_view' => 'smorken/importer::importer._form'])
