<?php
$attrs = $attrs ?? [];
?>
@foreach($attrs as $attr_key => $attr_value)
    @include('smorken/importer::_preset.input.__attr')
@endforeach
