@php
    $attrs = [
        'attrs' => [
            'name' => $name ?? 'textarea-'.rand(0, 1000),
            'class' => 'form-control '.($classes ?? ''),
            'placeholder' => $placeholder ?? false,
            'maxlength' => $maxlength ?? 2048,
            'rows' => $rows ?? 3
        ]
    ];
@endphp
<textarea @include('smorken/importer::_preset.input.__id')
        @include('smorken/importer::_preset.input.__attrs', $attrs)
        @include('smorken/importer::_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>{{ $value??old($name, (isset($model) && strlen($model->$name)) ? $model->$name : null) }}</textarea>
