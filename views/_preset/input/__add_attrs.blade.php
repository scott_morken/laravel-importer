<?php
$add_attrs = $add_attrs ?? [];
?>
@foreach($add_attrs as $attr_key => $attr_value)
    @include('smorken/importer::_preset.input.__attr')
@endforeach
