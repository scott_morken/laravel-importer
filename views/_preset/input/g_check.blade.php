<div class="form-check {{ $wrapper_classes??'' }}">
    @include('smorken/importer::_preset.input._checkbox')
    @include('smorken/importer::_preset.input._label', ['id' => ($id ?? $name).'-'.($value ?? 1), 'label_classes' => 'form-check-label'])
</div>
