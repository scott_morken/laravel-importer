<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImporterHRTables extends Migration
{

    protected $tables = [
        'departments',
        'emails',
        'jobs',
        'job_person',
        'locations',
        'people',
        'phones',
        'types',
    ];

    public function down()
    {
        foreach ($this->tables as $t) {
            Schema::dropIfExists($t);
        }
    }

    public function up()
    {
        Schema::create(
            'departments',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('primary_id', 32);
                $t->string('name', 64);
                $t->timestamps();
                $t->softDeletes();

                $t->unique('primary_id');
            }
        );

        Schema::create(
            'emails',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('person_id', 32);
                $t->string('email', 255);
                $t->boolean('system')->default(true);
                $t->timestamps();
                $t->softDeletes();

                $t->unique(['person_id', 'email'], 'em_pers_id_email_unq');
                $t->index('person_id', 'em_person_id_ndx');
                $t->index('email', 'em_email_ndx');
            }
        );

        Schema::create(
            'jobs',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('primary_id', 32);
                $t->string('type_id', 32);
                $t->string('name', 64);
                $t->timestamps();
                $t->softDeletes();

                $t->unique('primary_id');
                $t->index('type_id', 'jobs_type_id_ndx');
            }
        );

        Schema::create(
            'job_person',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('person_id', 32);
                $t->string('department_id', 32);
                $t->string('job_id', 32);
                $t->string('supervisor_id', 32);
                $t->string('location_id', 32);
                $t->boolean('manages')->default(false);
                $t->timestamps();
                $t->softDeletes();

                $t->index('person_id', 'jp_person_id_ndx');
                $t->index('department_id', 'jp_dept_id_ndx');
                $t->index('job_id', 'jp_job_id_ndx');
                $t->index('supervisor_id', 'jp_super_id_ndx');
                $t->index('location_id', 'jp_location_id_ndx');
                $t->index('manages', 'jp_manages_ndx');
                $t->unique(['person_id', 'department_id', 'job_id', 'location_id'], 'jp_per_job_unq');
            }
        );

        Schema::create(
            'locations',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('primary_id', 32);
                $t->string('name', 64);
                $t->timestamps();
                $t->softDeletes();

                $t->unique('primary_id');
            }
        );

        Schema::create(
            'people',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('primary_id', 32);
                $t->string('first_name', 64);
                $t->string('last_name', 64);
                $t->timestamps();
                $t->softDeletes();

                $t->unique('primary_id');
                $t->index('first_name', 'per_first_name_ndx');
                $t->index('last_name', 'per_last_name_ndx');
            }
        );

        Schema::create(
            'phones',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('person_id', 32);
                $t->string('phone', 16);
                $t->boolean('system')->default(true);
                $t->timestamps();
                $t->softDeletes();

                $t->index('person_id', 'pho_person_id_ndx');
                $t->index('phone', 'pho_phone_ndx');
                $t->unique(['person_id', 'phone'], 'ph_pers_id_phone_unq');
            }
        );

        Schema::create(
            'types',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('primary_id', 32);
                $t->string('name', 64);
                $t->timestamps();
                $t->softDeletes();

                $t->unique('primary_id');
            }
        );
    }
}
