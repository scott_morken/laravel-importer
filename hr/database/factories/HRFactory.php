<?php
$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Department::class,
    function (Faker\Generator $faker) {
        return [
            'primary_id' => 'D-'.$faker->randomNumber(5),
            'name' => $faker->words(2, true),
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Email::class,
    function (Faker\Generator $faker) {
        return [
            'person_id' => 'P-'.$faker->randomNumber(5),
            'email' => $faker->safeEmail,
            'system' => 1,
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Job::class,
    function (Faker\Generator $faker) {
        return [
            'primary_id' => 'J-'.$faker->randomNumber(5),
            'type_id' => 'T-'.$faker->randomNumber(5),
            'name' => $faker->words(2, true),
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\JobPerson::class,
    function (Faker\Generator $faker) {
        return [
            'person_id' => 'P-'.$faker->randomNumber(5),
            'department_id' => 'D-'.$faker->randomNumber(5),
            'job_id' => 'J-'.$faker->randomNumber(5),
            'supervisor_id' => 'P-'.$faker->randomNumber(5),
            'location_id' => 'L-'.$faker->randomNumber(5),
            'manages' => 0,
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Location::class,
    function (Faker\Generator $faker) {
        return [
            'primary_id' => 'L-'.$faker->randomNumber(5),
            'name' => $faker->words(2, true),
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Person::class,
    function (Faker\Generator $faker) {
        return [
            'primary_id' => 'P-'.$faker->randomNumber(5),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Phone::class,
    function (Faker\Generator $faker) {
        return [
            'person_id' => 'P-'.$faker->randomNumber(5),
            'phone' => $faker->phoneNumber,
            'system' => 1,
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\HR\Type::class,
    function (Faker\Generator $faker) {
        return [
            'primary_id' => 'T-'.$faker->randomNumber(5),
            'name' => $faker->words(2, true),
        ];
    }
);
