## Laravel Eloquent Importer

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.5+
* [Composer](https://getcomposer.org/)

### Composer
"smorken/importer": "~5.0"

### Use
* Add service providers to `config/app.php`
    * `\Smorken\Importer\ServiceProvider::class`
    * `\Smorken\Importer\CleanupServiceProvider::class`
* Add controller to `app\Http\routes.php`
    * `Route::controller('importer', '\Smorken\Importer\Http\Controllers\ImportController');`
    * Make sure to handle authentication/authorization
* Publish the config/views/db tags and run the database migrations
    * `php artisan vendor:publish --provider="Smorken\Importer\ServiceProvider" --tag=db --tag=config`
    * `php artisan migrate`
* Edit the config file `config/importer.php`
* Create your Import and Cleanup providers and add to the config

#### Import Examples
* `/src/Import/Providers/Examples`

#### Cleanup Examples
* `/srce/Import/Cleanup/Examples`

#### Artisan commands
* `php artisan importer:import`
* `php artisan importer:cleanup`
