<?php
$factory->define(
    Smorken\Importer\Models\Eloquent\Import::class,
    function (Faker\Generator $faker) {
        return [
            'provider_id' => 'dummy',
            'descr'       => $faker->words(5, true),
            'email_to'    => $faker->email,
            'active'      => 1,
        ];
    }
);

$factory->define(
    Smorken\Importer\Models\Eloquent\ImportRun::class,
    function (Faker\Generator $faker) {
        return [
            'import_id' => $faker->randomNumber(2),
            'results'   => new \Smorken\Importer\Models\VO\ImportResult(),
            'run_by_id' => 0,
        ];
    }
);
