<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImporterTables extends Migration
{

    public function up()
    {
        Schema::create(
            'imports',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('descr', 64);
                $t->string('provider_id', 32);
                $t->string('email_to', 128)->nullable();
                $t->boolean('active')->default(true);
                $t->timestamps();

                $t->index('provider_id', 'imp_provider_id_ndx');
            }
        );

        Schema::create(
            'import_runs',
            function (Blueprint $t) {
                $t->increments('id');
                $t->integer('import_id')->unsigned();
                $t->text('results')->nullable();
                $t->string('run_by_id', 32);
                $t->timestamps();

                $t->index('import_id', 'impr_import_id_ndx');
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('imports');
        Schema::dropIfExists('import_runs');
    }
}
