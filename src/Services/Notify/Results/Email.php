<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 8:36 AM
 */

namespace Smorken\Importer\Services\Notify\Results;

use Exception;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Mail\Mailer;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportRun;
use Smorken\Importer\Contracts\Notify\Results;
use Smorken\Importer\Mail\ImportResults;

class Email implements Results
{

    /**
     * @var Repository
     */
    protected $config;

    /**
     * @var ExceptionHandler
     */
    protected $exceptionHandler;

    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct(Mailer $mailer, Repository $config, ExceptionHandler $handler)
    {
        $this->config = $config;
        $this->mailer = $mailer;
        $this->exceptionHandler = $handler;
    }

    /**
     * @param  Import  $import
     * @param  ImportRun  $importRun
     * @param  string  $view
     * @return bool
     */
    public function send(Import $import, ImportRun $importRun, $view = 'smorken/importer::results.email')
    {
        if ($import->email_to) {
            $to = $import->email_to;
            $from = $this->getFrom();
            $subject = sprintf('Results for import: %s', $import->descr);
            $data = [
                'import' => $import,
                'import_run' => $importRun,
            ];
            try {
                $n = new ImportResults($data);
                $n->subject($subject);
                $n->view($view);
                $n->from($from);
                $this->mailer->to($to)->send($n);
                return true;
            } catch (Exception $e) {
                $this->exceptionHandler->report($e);
            }
            return false;
        }
        return true;
    }

    protected function getFrom()
    {
        $from = $this->config->get('importer.from');
        if (!$from) {
            $from = $this->config->get('mail.from.address');
        }
        return $from;
    }
}
