<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 6:54 AM
 */

namespace Smorken\Importer\Contracts\Models;

interface ImportResult
{

    /**
     * @param  string  $key
     * @param  mixed  $message
     * @return void
     */
    public function addMessage($key, $message);

    /**
     * @param  string  $counter
     * @return int
     */
    public function getCounter($counter);

    /**
     * @return array
     */
    public function getCounters();

    /**
     * @return Import
     */
    public function getImport();

    /**
     * @return array
     */
    public function getMessages();

    /**
     * @param  string  $counter
     * @param  int  $by
     */
    public function increment($counter, $by = 1);

    /**
     * @param  Import  $import
     * @param $counters
     */
    public function init(Import $import, $counters);

    public function initCounters($counters);

    public function initMessages();

    /**
     * @return static
     */
    public function newInstance();

    /**
     * @param  string  $counter
     */
    public function reset($counter);

    /**
     * @param $counter
     * @param $value
     * @return void
     */
    public function setCounter($counter, $value);

    /**
     * @param  Import  $import
     */
    public function setImport(Import $import);

    public function stop();

    /**
     * @return array
     */
    public function toArray();
}
