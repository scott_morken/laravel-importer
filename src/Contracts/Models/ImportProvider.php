<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 6:54 AM
 */

namespace Smorken\Importer\Contracts\Models;

use Smorken\Model\Contracts\VO\Model;

/**
 * Interface ImportProvider
 * @package Smorken\Importer\Contracts\Models
 *
 * @property string $id
 * @property string $name
 * @property string $provider
 */
interface ImportProvider extends Model
{

}
