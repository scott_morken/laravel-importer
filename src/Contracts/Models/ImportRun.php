<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/20/16
 * Time: 2:56 PM
 */

namespace Smorken\Importer\Contracts\Models;

/**
 * Interface ImportRun
 * @package Smorken\Importer\Contracts\Models
 *
 * @property int $id
 * @property int $import_id
 * @property ImportResult $result
 * @property string $run_by_id
 *
 * @property Import $import
 */
interface ImportRun
{

}
