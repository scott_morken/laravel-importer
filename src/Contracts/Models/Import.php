<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/20/16
 * Time: 2:56 PM
 */

namespace Smorken\Importer\Contracts\Models;

use Illuminate\Support\Collection;

/**
 * Interface Import
 * @package Smorken\Importer\Contracts\Models
 *
 * @property int $id
 * @property string $descr
 * @property string $provider_id
 * @property string $email_to
 * @property bool $active
 *
 * @property Collection<\Smorken\Importer\Contracts\Models\ImportRun> $runs
 * @property ImportProvider $importProvider
 */
interface Import
{

}
