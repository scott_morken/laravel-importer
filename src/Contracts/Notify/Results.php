<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:01 AM
 */

namespace Smorken\Importer\Contracts\Notify;

use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportRun;

interface Results
{

    /**
     * @param  Import  $import
     * @param  ImportRun  $importRun
     * @param  string  $view
     * @return bool
     */
    public function send(Import $import, ImportRun $importRun, $view = 'smorken/importer::results.email');
}
