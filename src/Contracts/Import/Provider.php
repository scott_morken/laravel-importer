<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 10:49 AM
 */

namespace Smorken\Importer\Contracts\Import;

use Illuminate\Database\DatabaseManager;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportResult;

interface Provider
{

    /**
     * @return ImportResult
     */
    public function getResult();

    /**
     * Entry point into the sync engine
     * @param  Import  $import
     * @return ImportResult
     */
    public function import(Import $import);

    /**
     * @param  ImportResult  $result
     * @param  DatabaseManager  $db
     * @return mixed
     */
    public function init(ImportResult $result, DatabaseManager $db);

    /**
     * Loads (can be chunked, etc) data from
     * the remote backend
     * @param  Import  $import
     * @return mixed
     */
    public function load(Import $import);

    /**
     * @param  Import  $import
     * @return array
     */
    public function sample(Import $import);
}
