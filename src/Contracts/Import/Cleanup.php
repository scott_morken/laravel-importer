<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 1:56 PM
 */

namespace Smorken\Importer\Contracts\Import;

interface Cleanup
{

    /**
     * @param  bool  $force
     * @return array
     */
    public function cleanup($force = false);

    /**
     * @param $key
     * @param  null  $default
     * @return mixed
     */
    public function getAttribute($key, $default = null);

    public function getProvider($contract, $return_model = false);

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function setAttribute($key, $value);

    public function setCompDate($date = '-14 days');

    public function setProvider($contract, $instance);

    public function setProviders($providers);
}
