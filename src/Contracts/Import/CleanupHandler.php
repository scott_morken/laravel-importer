<?php


namespace Smorken\Importer\Contracts\Import;


interface CleanupHandler
{

    public function addType($key, $concrete);

    public function getType($key);

    public function run($type, $force = false);

    public function runById($type, $id, $force = false);

    public function setAttribute($type, $key, $value);

    public function setAttributeById($type, $id, $key, $value);

    public function setCompDate($type, $date = '-14 days');

    public function setCompDateById($type, $id, $date = '-14 days');
}
