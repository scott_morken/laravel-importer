<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 8:40 AM
 */

namespace Smorken\Importer\Contracts\Import;

use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportResult;

interface Runner
{

    /**
     * @param  Import  $import
     * @return ImportResult
     */
    public function run(Import $import);

    /**
     * @param  Import  $import
     * @return array
     */
    public function sample(Import $import);
}
