<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 10:48 AM
 */

namespace Smorken\Importer\Contracts\Import;

use Illuminate\Support\Collection;
use Smorken\Importer\Contracts\Models\ImportResult;

interface Handler
{

    /**
     * @param  null  $id
     * @return Collection<ImportResult>
     */
    public function handle($id = null);
}
