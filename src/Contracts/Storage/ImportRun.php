<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:03 AM
 */

namespace Smorken\Importer\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Importer\Contracts\Models\ImportResult;

interface ImportRun
{

    /**
     * @param $id
     * @param  int  $count
     * @return Collection<\Smorken\Importer\Contracts\Models\ImportRun>
     */
    public function getByImportId($id, $count = 5);

    /**
     * @param $id
     * @return \Smorken\Importer\Contracts\Models\ImportRun
     */
    public function getLast($id);

    /**
     * @param  bool  $new
     * @return ImportResult
     */
    public function getResultModel($new = false);

    /**
     * @param  \Smorken\Importer\Contracts\Models\Import  $import
     * @param  ImportResult  $result
     * @return mixed
     */
    public function saveByImportAndResult(\Smorken\Importer\Contracts\Models\Import $import, ImportResult $result);
}
