<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:02 AM
 */

namespace Smorken\Importer\Contracts\Storage;

use Illuminate\Support\Collection;

interface Import
{

    /**
     * @return Collection<\Smorken\Importer\Contracts\Models\Import>
     */
    public function getActiveImports();
}
