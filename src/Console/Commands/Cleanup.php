<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:26 PM
 */

namespace Smorken\Importer\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class Cleanup extends Command
{

    use TimerTrait;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans up the import tables.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importer:cleanup';

    public function handle()
    {
        $this->outputStart();
        $this->line('Starting soft delete cleanup...');
        $this->doClean('cleanup.base');
        $this->line('Starting permanent cleanup...');
        $this->doClean('cleanup.permanent', true);
        $this->line('Starting orphan cleanup...');
        $this->doClean('cleanup.orphans', true);
        $this->outputFinished();
    }

    /**
     * @param  string  $key  DI name
     * @param  bool  $force
     */
    protected function doClean($key, $force = false)
    {
        $providers = App::make($key);
        $this->handleProviders($providers, $force);
    }

    /**
     * @param  Collection|array  $providers
     * @param $force
     */
    protected function handleProviders($providers, $force)
    {
        foreach ($providers as $contract => $provider) {
            $this->line("Provider: $contract");
            $results = $provider->cleanup($force);
            $this->outputResults($results);
        }
    }

    protected function outputResultSection($title, $section)
    {
        $this->line('** '.$title.' **');
        foreach ($section as $k => $count) {
            $this->line("  $k: $count");
        }
    }

    protected function outputResults($results)
    {
        $this->outputResultSection('Stale Records', $results['stale']);
        $this->outputResultSection('Force Deletes', $results['forceDelete']);
        $this->outputResultSection('Deletes', $results['delete']);
    }
}
