<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:26 PM
 */

namespace Smorken\Importer\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Importer\Contracts\Import\Handler;
use Smorken\Support\Arr;

class Import extends Command
{

    use TimerTrait;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the active imports';

    /**
     * @var Handler
     */
    protected $handler;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importer:import {--id=}';

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
        parent::__construct();
    }

    public function handle()
    {
        $this->outputStart();
        $results = $this->handler->handle($this->option('id'));
        foreach ($results as $id => $result) {
            $this->line("Results for ".$result->getImport()->descr);
            $counters = $result->getCounters();
            $this->line(Arr::stringify($counters));
        }
        $this->outputFinished();
    }
}
