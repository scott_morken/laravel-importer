<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:05 PM
 */

namespace Smorken\Importer\Storage\Eloquent;

use Illuminate\Support\Collection;

class Import extends Base implements \Smorken\Importer\Contracts\Storage\Import
{

    /**
     * @return Collection<\Smorken\Importer\Contracts\Models\Import>
     */
    public function getActiveImports()
    {
        return $this->getModel()
                    ->newQuery()
                    ->isActive()
                    ->defaultOrder()
                    ->get();
    }
}
