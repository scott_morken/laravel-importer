<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:09 PM
 */

namespace Smorken\Importer\Storage\Eloquent;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Smorken\Importer\Contracts\Models\ImportResult;

class ImportRun extends Base implements \Smorken\Importer\Contracts\Storage\ImportRun
{

    /**
     * @var ImportResult
     */
    protected $vo;

    /**
     * @param $model
     * @param $vo
     */
    public function __construct($model, $vo)
    {
        $this->vo = $vo;
        parent::__construct($model);
    }

    /**
     * @param $id
     * @param  int  $count
     * @return Collection<\Smorken\Importer\Contracts\Models\ImportRun>
     */
    public function getByImportId($id, $count = 5)
    {
        $q = $this->getModel()
                  ->newQuery()
                  ->where('import_id', '=', $id)
                  ->orderBy('created_at', 'DESC');
        return $this->limitOrPaginate($q, $count);
    }

    /**
     * @param $id
     * @return \Smorken\Importer\Contracts\Models\ImportRun
     */
    public function getLast($id)
    {
        return $this->getModel()
                    ->newQuery()
                    ->where('import_id', '=', $id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
    }

    /**
     * @param  bool  $new
     * @return ImportResult
     */
    public function getResultModel($new = false)
    {
        return ($new ? $this->vo->newInstance() : $this->vo);
    }

    /**
     * @param  \Smorken\Importer\Contracts\Models\Import  $import
     * @param  ImportResult  $result
     * @return mixed
     */
    public function saveByImportAndResult(\Smorken\Importer\Contracts\Models\Import $import, ImportResult $result)
    {
        $data = ['import_id' => $import->id, 'results' => $result, 'run_by_id' => 0];
        if (Auth::check()) {
            $data['run_by_id'] = Auth::id();
        }
        $m = $this->getModel()->newInstance($data);
        if ($m->save()) {
            return $m;
        }
    }
}
