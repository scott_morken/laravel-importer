<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/29/17
 * Time: 9:08 AM
 */

namespace Smorken\Importer\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ImportResults extends Mailable
{
    use SerializesModels;

    protected $payload = [];

    /**
     * Create a new message instance.
     *
     * @param  array  $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with($this->payload);
    }
}
