<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:56 AM
 */

namespace Smorken\Importer;

use Illuminate\Support\Collection;
use Smorken\Importer\Contracts\Import\CleanupHandler;
use Smorken\Importer\Import\Cleanup\Handler;

class CleanupServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CleanupHandler::class, function ($app) {
            $types = [
                'cleanup.base' => $this->getBaseHandlers($app),
                'cleanup.permanent' => $this->getPermanentHandlers($app),
                'cleanup.orphans' => $this->getOrphanHandlers($app),
            ];
            return new Handler($types);
        });
    }

    /**
     * @param $app
     * @return Collection
     */
    protected function getBaseHandlers($app)
    {
        $providers = $this->app['config']->get('importer_providers.cleanup.base', []);
        $time = $this->app['config']->get('importer.cleanup.base', '-14 days');
        return $this->getHandlers($providers, $time);
    }

    protected function getHandlers($providers, $time)
    {
        $handlers = new Collection();
        foreach ($providers as $provider_info) {
            $params = $provider_info['params'] ?? [];
            $params['look_back'] = $time;
            $handlers->put($provider_info['id'] ?? $provider_info['provider'],
                $this->app->make($provider_info['provider'], $params));
        }
        return $handlers;
    }

    /**
     * @param $app
     * @return Collection
     */
    protected function getOrphanHandlers($app)
    {
        $providers = $this->app['config']->get('importer_providers.cleanup.orphans', []);
        $time = $this->app['config']->get('importer.cleanup.orphans', '-14 days');
        return $this->getHandlers($providers, $time);
    }

    /**
     * @param $app
     * @return Collection
     */
    protected function getPermanentHandlers($app)
    {
        $providers = $this->app['config']->get('importer_providers.cleanup.permanent', []);
        $time = $this->app['config']->get('importer.cleanup.permanent', '-1 year');
        return $this->getHandlers($providers, $time);
    }
}
