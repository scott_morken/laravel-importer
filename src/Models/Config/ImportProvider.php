<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/29/17
 * Time: 9:19 AM
 */

namespace Smorken\Importer\Models\Config;

use Smorken\Model\Config;

class ImportProvider extends Config implements \Smorken\Importer\Contracts\Models\ImportProvider
{

    protected $config_key = 'importer_providers.importers';

    public function __toString()
    {
        return sprintf('%s', $this->name);
    }
}
