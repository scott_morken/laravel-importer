<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:32 PM
 */

namespace Smorken\Importer\Models\Eloquent;

use Smorken\Importer\Models\Eloquent\Traits\ImportProviderRelation;
use Smorken\Model\Traits\FriendlyColumns;

class Import extends Base implements \Smorken\Importer\Contracts\Models\Import
{

    use ImportProviderRelation, FriendlyColumns;

    protected $fillable = ['descr', 'provider_id', 'email_to', 'active'];

    /**
     * @return \Smorken\Importer\Contracts\Models\ImportRun|null
     */
    public function lastRun()
    {
        return $this->runs()->latest()->first();
    }

    public function rules()
    {
        $provider = $this->getImportProviderProvider();
        $allowed = $provider->all()->pluck('id')->all();
        return [
            'descr' => 'required',
            'provider_id' => 'required|in:'.implode(',', $allowed),
            'email_to' => 'email',
            'active' => 'boolean',
        ];
    }

    public function runs()
    {
        return $this->hasMany(ImportRun::class);
    }

    public function scopeDefaultOrder($query)
    {
        return $query->orderBy('active', 'desc')
                     ->orderBy('descr');
    }

    public function scopeIsActive($query)
    {
        return $query->where('active', '=', 1);
    }

    public function validationRules(array $override = []): array
    {
        return $this->mergeValidationRules($override, $this->rules());
    }
}
