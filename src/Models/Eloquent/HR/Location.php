<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Location extends Base implements \Smorken\Importer\Contracts\Models\HR\Location
{

    use SoftDeletes;

    protected $fillable = ['primary_id', 'name'];
}
