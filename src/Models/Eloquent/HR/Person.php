<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Person extends Base implements \Smorken\Importer\Contracts\Models\HR\Person
{

    use SoftDeletes;

    protected $fillable = ['primary_id', 'first_name', 'last_name'];

    public function jobPivots()
    {
        return $this->hasMany(JobPerson::class, 'person_id', 'primary_id');
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class, 'job_person', 'person_id', 'job_id', 'primary_id', 'primary_id')
                    ->withPivot('department_id', 'supervisor_id', 'location_id', 'manages');
    }

    public function managementJobs()
    {
        return $this->belongsToMany(Job::class, 'job_person', 'person_id', 'job_id', 'primary_id', 'primary_id')
                    ->withPivot('department_id', 'supervisor_id', 'location_id', 'manages')
                    ->wherePivot('manages', 1);
    }
}
