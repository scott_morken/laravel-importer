<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Type extends Base implements \Smorken\Importer\Contracts\Models\HR\Type
{

    use SoftDeletes;

    protected $fillable = ['primary_id', 'name'];
}
