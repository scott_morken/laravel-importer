<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class JobPerson extends Base implements \Smorken\Importer\Contracts\Models\HR\JobPerson
{

    use SoftDeletes;

    protected $fillable = [
        'person_id', 'department_id', 'job_id', 'supervisor_id', 'location_id', 'manages',
    ];

    protected $table = 'job_person';

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'primary_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id', 'primary_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'primary_id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id', 'primary_id');
    }

    public function supervisor()
    {
        return $this->belongsTo(Person::class, 'supervisor_id', 'primary_id');
    }
}
