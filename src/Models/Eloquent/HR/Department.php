<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Department extends Base implements \Smorken\Importer\Contracts\Models\HR\Department
{

    use SoftDeletes;

    protected $fillable = ['primary_id', 'name'];

    public function people()
    {
        return $this->belongsToMany(Person::class, 'job_person', 'primary_key', 'department_id', 'person_id',
            'primary_id')
                    ->withPivot('department_id', 'supervisor_id', 'location_id', 'manages');
    }
}
