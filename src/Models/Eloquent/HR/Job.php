<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Job extends Base implements \Smorken\Importer\Contracts\Models\HR\Job
{

    use SoftDeletes;

    protected $fillable = ['primary_id', 'type_id', 'name'];
}
