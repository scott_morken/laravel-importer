<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Phone extends Base implements \Smorken\Importer\Contracts\Models\HR\Phone
{

    use SoftDeletes;

    protected $fillable = ['person_id', 'phone', 'system'];

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id', 'primary_id');
    }
}
