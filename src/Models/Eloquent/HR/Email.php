<?php


namespace Smorken\Importer\Models\Eloquent\HR;


use Illuminate\Database\Eloquent\SoftDeletes;
use Smorken\Importer\Models\Eloquent\Base;

class Email extends Base implements \Smorken\Importer\Contracts\Models\HR\Email
{

    use SoftDeletes;

    protected $fillable = ['person_id', 'email', 'system'];

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id', 'primary_id');
    }
}
