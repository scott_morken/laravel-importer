<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:30 PM
 */

namespace Smorken\Importer\Models\Eloquent\Traits;

use Illuminate\Support\Facades\App;
use Smorken\Importer\Contracts\Storage\ImportProvider;

trait ImportProviderRelation
{

    /**
     * @var ImportProvider
     */
    protected $importProvider;

    /**
     * @var \Smorken\Importer\Contracts\Models\ImportProvider
     */
    protected $importProviderModel;

    /**
     * @return \Smorken\Importer\Contracts\Models\ImportProvider
     */
    public function getImportProviderAttribute()
    {
        if ($this->provider_id) {
            $this->importProviderModel = $this->getImportProviderProvider()->find($this->provider_id);
        }
        return $this->importProviderModel;
    }

    /**
     * @param  ImportProvider  $importProvider
     */
    public function setImportProviderProvider($importProvider)
    {
        $this->importProvider = $importProvider;
    }

    /**
     * @return ImportProvider
     */
    protected function getImportProviderProvider()
    {
        if (!$this->importProvider) {
            $this->importProvider = App::make(ImportProvider::class);
        }
        return $this->importProvider;
    }
}
