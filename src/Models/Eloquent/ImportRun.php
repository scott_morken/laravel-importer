<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:34 PM
 */

namespace Smorken\Importer\Models\Eloquent;

class ImportRun extends Base implements \Smorken\Importer\Contracts\Models\ImportRun
{

    protected $fillable = ['import_id', 'results', 'run_by_id'];

    public function __toString()
    {
        return sprintf('%d: %d at %s', $this->id, $this->import_id, $this->created_at);
    }

    public function getResultsAttribute()
    {
        return unserialize($this->attributes['results'] ?? null);
    }

    public function setResultsAttribute($v)
    {
        $this->attributes['results'] = serialize($v);
    }
}
