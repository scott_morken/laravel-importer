<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 6:53 AM
 */

namespace Smorken\Importer\Models\VO;

use Smorken\Model\VO;

class ImportProvider extends VO implements \Smorken\Importer\Contracts\Models\ImportProvider
{

    public function __toString()
    {
        return sprintf('%s', $this->name);
    }
}
