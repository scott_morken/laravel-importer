<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/20/16
 * Time: 2:54 PM
 */

namespace Smorken\Importer\Models\VO;

use Carbon\Carbon;
use Smorken\Importer\Contracts\Models\Import;

class ImportResult implements \Smorken\Importer\Contracts\Models\ImportResult
{

    public $counters = [];

    public $end;

    public $import;

    public $mem;

    public $messages = [];

    public $started;

    /**
     * @inheritDoc
     */
    public function addMessage($key, $message)
    {
        $this->messages[] = [$key => $message];
    }

    /**
     * @param  string  $counter
     * @return int
     */
    public function getCounter($counter)
    {
        return $this->counters[$counter] ?? 0;
    }

    /**
     * @return array
     */
    public function getCounters()
    {
        return $this->counters;
    }

    /**
     * @return Import
     */
    public function getImport()
    {
        return $this->import;
    }

    /**
     * @param  Import  $import
     */
    public function setImport(Import $import)
    {
        $this->import = $import;
    }

    /**
     * @inheritDoc
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param  string  $counter
     * @param  int  $by
     */
    public function increment($counter, $by = 1)
    {
        if (!isset($this->counters[$counter])) {
            $this->reset($counter);
        }
        $current = $this->getCounter($counter);
        $this->setCounter($counter, ($current + $by));
    }

    /**
     * @param  Import  $import
     * @param $counters
     */
    public function init(Import $import, $counters)
    {
        $this->setImport($import);
        $this->initCounters($counters);
        $this->initMessages();
        $this->started = Carbon::now();
    }

    public function initCounters($counters)
    {
        $this->counters = [];
        foreach ($counters as $counter) {
            $this->reset($counter);
        }
    }

    public function initMessages()
    {
        $this->messages = [];
    }

    /**
     * @return static
     */
    public function newInstance()
    {
        return new static();
    }

    /**
     * @param  string  $counter
     */
    public function reset($counter)
    {
        $this->setCounter($counter, 0);
    }

    /**
     * @inheritDoc
     */
    public function setCounter($counter, $value)
    {
        $this->counters[$counter] = $value;
    }

    public function stop()
    {
        $this->end = Carbon::now();
        $this->mem = $this->getMemoryUse();
    }

    public function toArray()
    {
        $data = $this->getCounters();
        $data['start_time'] = $this->started;
        $data['end_time'] = $this->end;
        $data['memory'] = $this->mem;
        $data['messages'] = $this->messages;
        return $data;
    }

    protected function getMemoryUse(): float
    {
        return memory_get_peak_usage(true) / pow(2, 20);
    }
}
