<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 7:57 AM
 */

use Illuminate\Support\Facades\Config;
use Smorken\Importer\Http\Controllers\ImportController;
use Smorken\Support\Routes;

Route::group(
    [
        'middleware' => Config::get('importer.middleware',
            ['web', 'auth', 'can:role-admin']),
        'prefix' => Config::get('importer.route_prefix', 'admin'),
    ],
    function () {
        Route::group(
            [
                'prefix' => 'importer',
            ],
            function () {
                $controller = Config::get('importer.controller',
                    ImportController::class);
                Routes::create($controller, [], [
                    'get|cleanup' => 'cleanup',
                    'get|run/{id}' => 'run',
                    'get|run-view/{import_id}/{id?}' => 'runView',
                    'get|sample/{id}' => 'sample',
                ]);
            }
        );
    }
);
