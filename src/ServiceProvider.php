<?php namespace Smorken\Importer;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Mail\Mailer;
use Smorken\Importer\Console\Commands\Cleanup;
use Smorken\Importer\Contracts\Import\Handler;
use Smorken\Importer\Contracts\Import\Runner;
use Smorken\Importer\Contracts\Notify\Results;
use Smorken\Importer\Contracts\Storage\Import;
use Smorken\Importer\Contracts\Storage\ImportRun;
use Smorken\Importer\Services\Notify\Results\Email;
use Smorken\Support\Contracts\Binder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->bootConfig();
        $this->bootProvidersConfig();
        $this->bootViews();
        $this->bootFactory(__DIR__.'/../database/factories');
        $this->loadRoutes();
        $this->bindStorageFromArray($this->getStorageArray());
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bindNotifier();
        $this->bindRunner();
        $this->bindHandler();
        $this->commands(
            [
                Console\Commands\Import::class,
                Cleanup::class,
            ]
        );
    }

    protected function bindHandler()
    {
        $this->app->bind(
            Handler::class,
            function ($app) {
                $runner = $app[Runner::class];
                $ip = $app[Import::class];
                return new \Smorken\Importer\Import\Handler($runner, $ip);
            }
        );
    }

    protected function bindNotifier()
    {
        $this->app->bind(
            Results::class,
            function ($app) {
                $mailer = $app[Mailer::class];
                $config = $app['config'];
                $eh = $app[ExceptionHandler::class];
                return new Email($mailer, $config, $eh);
            }
        );
    }

    protected function bindRunner()
    {
        $this->app->bind(
            Runner::class,
            function ($app) {
                $irp = $app[ImportRun::class];
                $results = $app[Results::class];
                $db = $app['db'];
                return new \Smorken\Importer\Import\Runner($irp, $results, $db);
            }
        );
    }

    protected function bindStorageFromArray($storage)
    {
        $binder = $this->app->make(Binder::class);
        $binder->bindAll($storage);
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'importer');
        $this->publishes([$config => config_path('importer.php')], 'config');
    }

    protected function bootFactory($path)
    {
        if ($this->app->bound('Illuminate\Database\Eloquent\Factory') && class_exists('Faker\Generator')) {
            $this->app->make('Illuminate\Database\Eloquent\Factory')
                      ->load($path);
        }
    }

    protected function bootProvidersConfig()
    {
        $config = __DIR__.'/../config/providers.php';
        $this->mergeConfigFrom($config, 'importer_providers');
        $this->publishes([$config => config_path('importer_providers.php')], 'config');
    }

    protected function bootViews()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/importer');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/smorken/importer'),
            ],
            'views'
        );
    }

    protected function getStorageArray()
    {
        return $this->app['config']->get('importer.storage', []);
    }

    protected function loadRoutes()
    {
        if ($this->app['config']->get('importer.load_routes', true)) {
            require __DIR__.'/routes.php';
        }
    }
}
