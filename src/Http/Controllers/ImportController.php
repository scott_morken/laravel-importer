<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 7:07 AM
 */

namespace Smorken\Importer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Smorken\Ext\Controller\BaseController;
use Smorken\Ext\Controller\Traits\Full;
use Smorken\Ext\Controller\Traits\IndexFiltered;
use Smorken\Importer\Contracts\Import\CleanupHandler;
use Smorken\Importer\Contracts\Import\Runner;
use Smorken\Importer\Contracts\Storage\Import;
use Smorken\Importer\Contracts\Storage\ImportProvider;
use Smorken\Importer\Contracts\Storage\ImportRun;
use Smorken\Importer\Http\Requests\ImportForm;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImportController extends BaseController
{

    use Full, IndexFiltered {
        create as traitedCreate;
        doSave as traitedDoSave;
        update as traitedUpdate;
        view as traitedView;
    }

    protected $base_view = 'smorken/importer::importer';

    /**
     * @var CleanupHandler
     */
    protected $cleanupHandler;

    /**
     * @var ImportProvider
     */
    protected $importProvider;

    /**
     * @var ImportRun
     */
    protected $importRun;

    protected $subnav = 'admin';

    public function __construct(
        CleanupHandler $cleanupHandler,
        Import $provider,
        ImportProvider $importProvider,
        ImportRun $importRun
    ) {
        $this->cleanupHandler = $cleanupHandler;
        $this->importProvider = $importProvider;
        $this->importRun = $importRun;
        $this->setProvider($provider);
        parent::__construct();
    }

    public function cleanup()
    {
        $results = [
            'base' => $this->cleanupHandler->run('cleanup.base'),
            'permanent' => $this->cleanupHandler->run('cleanup.permanent', true),
            'orphans' => $this->cleanupHandler->run('cleanup.orphans', true),
        ];
        return View::make($this->getView('cleanup'))
                   ->with('results', $results);
    }

    public function create(Request $request)
    {
        return $this->addImportProvidersToResponse($this->traitedCreate($request));
    }

    public function doSave(ImportForm $request, $id = null)
    {
        return $this->traitedDoSave($request, $id);
    }

    public function run(Request $request, Runner $runner, $id)
    {
        $import = $this->findModel($id);
        $results = $runner->run($import);
        $request->session()->flash('success', 'Import ran for '.$import->descr);
        return Redirect::action($this->actionArray('runView'), ['import_id' => $id]);
    }

    public function runView(Request $request, $import_id, $run_id = null)
    {
        $import = $this->findModel($import_id);
        if ($run_id) {
            $run = $this->importRun->find($run_id);
            if (!$run) {
                throw new NotFoundHttpException("Unable to locate [$run_id].");
            }
        } else {
            $run = $this->importRun->getLast($import_id);
        }
        return View::make($this->getView('run_view'))
                   ->with('import', $import)
                   ->with('run', $run);
    }

    public function sample(Request $request, Runner $runner, $id)
    {
        $import = $this->findModel($id);
        $results = $runner->sample($import);
        return View::make($this->getView('sample'), ['id' => $id])->with('results', $results)->with('import', $import);
    }

    public function update(Request $request, $id)
    {
        return $this->addImportProvidersToResponse($this->traitedUpdate($request, $id));
    }

    public function view(Request $request, $id)
    {
        return $this->addImportResultsToResponse($this->traitedView($request, $id), $id);
    }

    protected function addImportProvidersToResponse($response)
    {
        $import_providers = $this->importProvider->all();
        return $response->with('import_providers', $import_providers);
    }

    protected function addImportResultsToResponse($response, $id)
    {
        $imports = $this->getLastImportResults($id);
        return $response->with('imports', $imports);
    }

    protected function getLastImportResults($import_id)
    {
        return $this->importRun->getByImportId($import_id, 5);
    }
}
