<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 10:47 AM
 */

namespace Smorken\Importer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Smorken\Importer\Contracts\Storage\Import;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class ImportForm extends FormRequest
{

    use SanitizeRequest;

    public function rules(Import $provider)
    {
        return $provider->validationRules();
    }

    public function validator(Import $import, Sanitize $sanitize, $factory)
    {
        $this->sanitize($sanitize, $this, [
            'descr' => 'string',
            'provider_id' => 'alphaNumDash',
            'email_to' => 'email',
            'active' => 'bool',
        ]);
        return $factory->make(
            $this->validationData(),
            $this->rules($import),
            $this->messages(),
            $this->attributes()
        );
    }
}
