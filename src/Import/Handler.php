<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:23 AM
 */

namespace Smorken\Importer\Import;

use Illuminate\Support\Collection;
use Smorken\Importer\Contracts\Models\ImportResult;
use Smorken\Importer\Contracts\Storage\Import;

class Handler implements \Smorken\Importer\Contracts\Import\Handler
{

    /**
     * @var Import
     */
    protected $import_provider;

    /**
     * @var Collection
     */
    protected $results;

    /**
     * @var \Smorken\Importer\Contracts\Import\Runner
     */
    protected $runner;

    public function __construct(
        \Smorken\Importer\Contracts\Import\Runner $runner,
        Import $import_provider
    ) {
        $this->setRunner($runner);
        $this->setImportProvider($import_provider);
        $this->results = new Collection();
    }

    /**
     * @return Import
     */
    public function getImportProvider()
    {
        return $this->import_provider;
    }

    /**
     * @param  Import  $import_provider
     */
    public function setImportProvider($import_provider)
    {
        $this->import_provider = $import_provider;
    }

    /**
     * @return \Smorken\Importer\Contracts\Import\Runner
     */
    public function getRunner()
    {
        return $this->runner;
    }

    /**
     * @param  \Smorken\Importer\Contracts\Import\Runner  $runner
     */
    public function setRunner($runner)
    {
        $this->runner = $runner;
    }

    /**
     * @param  null  $id
     * @return Collection<\Smorken\Importer\Contracts\Models\ImportResult>
     */
    public function handle($id = null)
    {
        $imports = $this->getImports($id);
        foreach ($imports as $import) {
            $this->results->put($import->id, $this->getRunner()->run($import));
        }
        return $this->results;
    }

    protected function getImports($id = null)
    {
        $imports = new Collection();
        if ($id) {
            $import = $this->getImportProvider()->find($id);
            if ($import) {
                $imports->push($import);
            }
        } else {
            $imports = $this->getImportProvider()->getActiveImports();
        }
        return $imports;
    }
}
