<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:29 AM
 */

namespace Smorken\Importer\Import\Providers;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\App;
use Smorken\Importer\Contracts\Import\Provider;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportResult;

abstract class Base implements Provider
{

    protected $counter_defaults = [
        'errors',
    ];

    /**
     * @var DatabaseManager
     */
    protected $db;

    protected $providers = [];

    /**
     * @var ImportResult
     */
    protected $result;

    /**
     * @return DatabaseManager
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param  DatabaseManager  $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return ImportResult
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param  ImportResult  $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * Entry point into the sync engine
     *
     * @param  Import  $import
     * @return ImportResult
     */
    public function import(Import $import)
    {
        $this->getResult()->init($import, $this->counter_defaults);
        $this->load($import);
        $this->getResult()->stop();
        return $this->getResult();
    }

    public function init(ImportResult $result, DatabaseManager $db)
    {
        $this->setResult($result);
        $this->setDb($db);
    }

    public function sample(Import $import)
    {
        return $this->loadSample($import);
    }

    protected function dataOperation($counter, $table, $keys, $upd_cols, $data)
    {
        $r = 0;
        if ($data) {
            $connection = null;
            if (is_object($table)) {
                $connection = $table->getConnectionName();
                $table = $table->getTable();
            }
            $r = $this->getDb()->connection($connection)->table($table)->createOrUpdate($keys, $data, $upd_cols);
        }
        $this->getResult()->increment($counter.'::total', count($data));
        $this->getResult()->increment($counter.'::updated_or_created', $r);
    }

    protected function dataOperationViaTemp($counter, $table, $temp, $keys, $ins_cols, $upd_cols)
    {
        $connection = null;
        if (is_object($table)) {
            $connection = $table->getConnectionName();
            $table = $table->getTable();
        }
        $this->getResult()->increment(
            $counter.'::updated',
            $this->getDb()->connection($connection)->table($table)->updateFrom(
                $temp,
                $keys,
                $upd_cols
            )
        );
        $this->getResult()->increment(
            $counter.'::created',
            $this->getDb()->connection($connection)->table($table)->insertFrom(
                $temp,
                $keys,
                $ins_cols
            )
        );
    }

    protected function getModel($contract, $return_provider = false)
    {
        if (!isset($this->providers[$contract])) {
            $this->providers[$contract] = App::make($contract);
        }
        $p = $this->providers[$contract] ?? null;
        if ($return_provider) {
            return $p;
        }
        return $p->getModel();
    }

    protected function insertIntoTemp($counter, $table, string $temp, array $data)
    {
        $connection = null;
        if (is_object($table)) {
            $connection = $table->getConnectionName();
        }
        $this->getResult()->increment($counter.'::total', count($data));
        return $this->getDb()->connection($connection)->table($temp)->insert($data);
    }

    protected function loadSample(Import $import)
    {
        return [];
    }
}
