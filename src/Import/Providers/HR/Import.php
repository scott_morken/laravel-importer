<?php


namespace Smorken\Importer\Import\Providers\HR;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Smorken\Importer\Contracts\Import\Provider;
use Smorken\Importer\Contracts\Storage\HR\Department;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\Job;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Location;
use Smorken\Importer\Contracts\Storage\HR\Person;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Contracts\Storage\HR\Type;
use Smorken\Importer\Import\Providers\Base;

class Import extends Base implements Provider
{

    protected $counter_defaults = [
        'person::total',
        'location::total',
        'department::total',
        'job::total',
        'job_person::total',
        'type::total',
        'email::total',
        'phone::total',
        'person::updated_or_created',
        'location::updated_or_created',
        'department::updated_or_created',
        'job::updated_or_created',
        'job_person::updated_or_created',
        'type::updated_or_created',
        'email::updated_or_created',
        'phone::updated_or_created',
        'errors',
    ];

    /**
     * @var \Smorken\Storage\Contracts\Storage\Base
     */
    protected $hrProvider;

    protected $ids = [
        'persons' => [],
        'locations' => [],
        'depts' => [],
        'types' => [],
        'jobs' => [],
        'dept_persons' => [],
        'job_persons' => [],
        'emails' => [],
        'phones' => [],
    ];

    public function __construct($hrProvider)
    {
        if (is_string($hrProvider)) {
            $hrProvider = App::make($hrProvider);
        }
        $this->hrProvider = $hrProvider;
    }

    /**
     * Loads (can be chunked, etc) data from
     * the remote backend
     * @param  \Smorken\Importer\Contracts\Models\Import  $import
     * @return mixed
     */
    public function load(\Smorken\Importer\Contracts\Models\Import $import)
    {
        $this->hrProvider->getActiveEmployees(
            function ($models) use ($import) {
                $this->update($import, $models);
            }
        );
    }

    protected function update(\Smorken\Importer\Contracts\Models\Import $import, $models)
    {
        $chunk = $this->getChunk($import, $models);
        $this->handleDataOperations($chunk);
    }

    protected function addDeptData($import, $model, $depts)
    {
        if ($model && $model->department_id && !$this->idExists('depts', $model->department_id)) {
            $this->addId('depts', $model->department_id);
            $depts[$model->department_id] = [
                'primary_id' => $model->department_id,
                'name' => $model->department,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $depts;
    }

    protected function addEmailData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->email]);
        if ($model && $model->email && !$this->idExists('emails', $key)) {
            $this->addId('emails', $key);
            $contains[$key] = [
                'person_id' => $model->id,
                'email' => $model->email,
                'system' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $contains;
    }

    protected function addId($key, $id)
    {
        $this->ids[$key][$id] = true;
    }

    protected function addJobData($import, $model, $jobs)
    {
        if ($model &&
            $model->job_id &&
            !$this->idExists('jobs', $model->jobs_id)
        ) {
            $this->addId('jobs', $model->job_id);
            $jobs[$model->job_id] = [
                'primary_id' => $model->job_id,
                'type_id' => $model->type_id,
                'name' => $model->job_desc,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $jobs;
    }

    protected function addJobPersonData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->department_id, $model->location_id, $model->job_id]);
        if ($model &&
            $model->id && $model->department_id && $model->location_id && $model->job_id &&
            !$this->idExists(
                'job_persons',
                $key
            )
        ) {
            $this->addId('job_persons', $key);
            $manages = $model->manages_department_id === $model->department_id;
            $contains[$key] = [
                'person_id' => $model->id,
                'department_id' => $model->department_id,
                'job_id' => $model->job_id,
                'supervisor_id' => $model->supervisor_id,
                'location_id' => $model->location_id,
                'manages' => $manages,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $contains;
    }

    protected function addLocationData($import, $model, $locs)
    {
        if ($model && $model->location_id && !$this->idExists('locations', $model->location_id)) {
            $this->addId('locations', $model->location_id);
            $locs[$model->location_id] = [
                'primary_id' => $model->location_id,
                'name' => $model->location,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $locs;
    }

    protected function addPersonData($import, $model, $persons)
    {
        if ($model && $model->id && !$this->idExists('persons', $model->id)) {
            $this->addId('persons', $model->id);
            $persons[$model->id] = [
                'primary_id' => $model->id,
                'first_name' => $model->first_name,
                'last_name' => $model->last_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $persons;
    }

    protected function addPhoneData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->phone]);
        if ($model && $model->phone && !$this->idExists('phones', $key)) {
            $this->addId('phones', $key);
            $phone = $this->normalizePhone($model->phone);
            if ($phone) {
                $contains[$phone] = [
                    'person_id' => $model->id,
                    'phone' => $phone,
                    'system' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => null,
                ];
            }
        }
        return $contains;
    }

    protected function addTypeData($import, $model, $types)
    {
        if ($model && $model->type_id && !$this->idExists('types', $model->type_id)) {
            $this->addId('types', $model->type_id);
            $types[$model->type_id] = [
                'primary_id' => $model->type_id,
                'name' => $model->job_type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $types;
    }

    protected function dataOperationDept($models)
    {
        $this->dataOperation(
            'department',
            $this->getDept(),
            [
                'primary_id',
            ],
            ['name', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationEmail($models)
    {
        $this->dataOperation(
            'email',
            $this->getEmail(),
            [
                'person_id',
                'email',
            ],
            ['email', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationJob($models)
    {
        $this->dataOperation(
            'job',
            $this->getJob(),
            [
                'primary_id',
            ],
            ['type_id', 'name', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationJobPerson($models)
    {
        $this->dataOperation(
            'job_person',
            $this->getJobPerson(),
            [
                'person_id',
                'department_id',
                'job_id',
                'location_id',
            ],
            ['updated_at', 'supervisor_id', 'manages'],
            $models
        );
    }

    protected function dataOperationLocation($models)
    {
        $this->dataOperation(
            'location',
            $this->getLocation(),
            [
                'primary_id',
            ],
            ['name', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationPerson($models)
    {
        $this->dataOperation(
            'person',
            $this->getPerson(),
            [
                'primary_id',
            ],
            ['last_name', 'first_name', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationPhone($models)
    {
        $this->dataOperation(
            'phone',
            $this->getPhone(),
            [
                'person_id',
                'phone',
            ],
            ['phone', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function dataOperationType($models)
    {
        $this->dataOperation(
            'type',
            $this->getType(),
            [
                'primary_id',
            ],
            ['name', 'deleted_at', 'updated_at'],
            $models
        );
    }

    protected function getChunk(\Smorken\Importer\Contracts\Models\Import $import, Collection $models)
    {
        $chunk = [
            'persons' => [],
            'locations' => [],
            'depts' => [],
            'types' => [],
            'jobs' => [],
            'dept_persons' => [],
            'job_persons' => [],
            'emails' => [],
            'phones' => [],
        ];
        foreach ($models as $model) {
            $chunk['persons'] = $this->addPersonData($import, $model, $chunk['persons']);
            $chunk['locations'] = $this->addLocationData($import, $model, $chunk['locations']);
            $chunk['depts'] = $this->addDeptData($import, $model, $chunk['depts']);
            $chunk['types'] = $this->addTypeData($import, $model, $chunk['types']);
            $chunk['jobs'] = $this->addJobData($import, $model, $chunk['jobs']);
            $chunk['job_persons'] = $this->addJobPersonData($import, $model, $chunk['job_persons']);
            $chunk['emails'] = $this->addEmailData($import, $model, $chunk['emails']);
            $chunk['phones'] = $this->addPhoneData($import, $model, $chunk['phones']);
        }
        return $chunk;
    }

    protected function getDept($return_provider = false)
    {
        return $this->getModel(Department::class, $return_provider);
    }

    protected function getEmail($return_provider = false)
    {
        return $this->getModel(Email::class, $return_provider);
    }

    protected function getJob($return_provider = false)
    {
        return $this->getModel(Job::class, $return_provider);
    }

    protected function getJobPerson($return_provider = false)
    {
        return $this->getModel(JobPerson::class, $return_provider);
    }

    protected function getLocation($return_provider = false)
    {
        return $this->getModel(Location::class, $return_provider);
    }

    protected function getPerson($return_provider = false)
    {
        return $this->getModel(Person::class, $return_provider);
    }

    protected function getPhone($return_provider = false)
    {
        return $this->getModel(Phone::class, $return_provider);
    }

    protected function getType($return_provider = false)
    {
        return $this->getModel(Type::class, $return_provider);
    }

    protected function handleDataOperations($chunk)
    {
        $this->dataOperationPerson($chunk['persons']);
        $this->dataOperationLocation($chunk['locations']);
        $this->dataOperationDept($chunk['depts']);
        $this->dataOperationType($chunk['types']);
        $this->dataOperationJob($chunk['jobs']);
        $this->dataOperationJobPerson($chunk['job_persons']);
        $this->dataOperationEmail($chunk['emails']);
        $this->dataOperationPhone($chunk['phones']);
    }

    protected function idExists($key, $id)
    {
        return array_key_exists($id, $this->ids[$key]);
    }

    protected function loadSample(\Smorken\Importer\Contracts\Models\Import $import)
    {
        $chunk = [];
        $this->hrProvider->getActiveEmployees(
            function ($models) use ($import, &$chunk) {
                $chunk = $this->getChunk($import, $models);
            }
        );
        return $chunk;
    }

    protected function normalizePhone($phone)
    {
        $phone = preg_replace('/[^\d]/', '', $phone);
        if ($phone === '0000000000') {
            return null;
        }
        if (strlen($phone) === 10) {
            return preg_replace('/(\d{3})(\d{3})(\d{4})/', '($1) $2-$3', $phone);
        } elseif (strlen($phone) === 7) {
            return preg_replace('/(\d{3})(\d{4})/', '$1-$2', $phone);
        }
        return $phone;
    }
}
