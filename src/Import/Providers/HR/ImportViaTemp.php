<?php

namespace Smorken\Importer\Import\Providers\HR;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Smorken\Importer\Contracts\Import\Provider;
use Smorken\Importer\Contracts\Storage\HR\Department;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\Job;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Location;
use Smorken\Importer\Contracts\Storage\HR\Person;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Contracts\Storage\HR\Type;
use Smorken\Importer\Import\Providers\Base;

class ImportViaTemp extends Base implements Provider
{

    /**
     * @var \Smorken\Storage\Contracts\Storage\Base
     */
    protected $hrProvider;

    protected $ids = [
        'persons' => [],
        'locations' => [],
        'depts' => [],
        'types' => [],
        'jobs' => [],
        'dept_persons' => [],
        'job_persons' => [],
        'emails' => [],
        'phones' => [],
    ];

    protected $tableMap = [
        'persons' => 'getPerson',
        'locations' => 'getLocation',
        'depts' => 'getDept',
        'types' => 'getType',
        'jobs' => 'getJob',
        'job_persons' => 'getJobPerson',
        'emails' => 'getEmail',
        'phones' => 'getPhone',
    ];

    protected $tempTables = [
        'getDept' => null,
        'getEmail' => null,
        'getJob' => null,
        'getJobPerson' => null,
        'getLocation' => null,
        'getPerson' => null,
        'getPhone' => null,
        'getType' => null,
    ];

    public function __construct($hrProvider)
    {
        if (is_string($hrProvider)) {
            $hrProvider = App::make($hrProvider);
        }
        $this->hrProvider = $hrProvider;
    }

    /**
     * Loads (can be chunked, etc) data from
     * the remote backend
     *
     * @param  \Smorken\Importer\Contracts\Models\Import  $import
     * @return mixed
     */
    public function load(\Smorken\Importer\Contracts\Models\Import $import)
    {
        $this->createTempTables();
        $this->hrProvider->getActiveEmployees(
            function ($models) use ($import) {
                $this->update($import, $models);
            }
        );
        $this->handleDataOperations();
        $this->dropTempTables();
    }

    protected function update(\Smorken\Importer\Contracts\Models\Import $import, $models)
    {
        $chunk = $this->getChunk($import, $models);
        $this->handleTempDataOperations($chunk);
    }

    protected function addDeptData($import, $model, $depts)
    {
        if ($model && $model->department_id && !$this->idExists('depts', $model->department_id)) {
            $this->addId('depts', $model->department_id);
            $depts[$model->department_id] = [
                'primary_id' => $model->department_id,
                'name' => $model->department,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $depts;
    }

    protected function addEmailData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->email]);
        if ($model && $model->email && !$this->idExists('emails', $key)) {
            $this->addId('emails', $key);
            $contains[$key] = [
                'person_id' => $model->id,
                'email' => $model->email,
                'system' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $contains;
    }

    protected function addId($key, $id)
    {
        $this->ids[$key][$id] = true;
    }

    protected function addJobData($import, $model, $jobs)
    {
        if ($model &&
            $model->job_id &&
            !$this->idExists('jobs', $model->jobs_id)
        ) {
            $this->addId('jobs', $model->job_id);
            $jobs[$model->job_id] = [
                'primary_id' => $model->job_id,
                'type_id' => $model->type_id,
                'name' => $model->job_desc,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $jobs;
    }

    protected function addJobPersonData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->department_id, $model->location_id, $model->job_id]);
        if ($model &&
            $model->id && $model->department_id && $model->location_id && $model->job_id &&
            !$this->idExists(
                'job_persons',
                $key
            )
        ) {
            $this->addId('job_persons', $key);
            $manages = $model->manages_department_id === $model->department_id;
            $contains[$key] = [
                'person_id' => $model->id,
                'department_id' => $model->department_id,
                'job_id' => $model->job_id,
                'supervisor_id' => $model->supervisor_id,
                'location_id' => $model->location_id,
                'manages' => $manages,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $contains;
    }

    protected function addLocationData($import, $model, $locs)
    {
        if ($model && $model->location_id && !$this->idExists('locations', $model->location_id)) {
            $this->addId('locations', $model->location_id);
            $locs[$model->location_id] = [
                'primary_id' => $model->location_id,
                'name' => $model->location,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $locs;
    }

    protected function addPersonData($import, $model, $persons)
    {
        if ($model && $model->id && !$this->idExists('persons', $model->id)) {
            $this->addId('persons', $model->id);
            $persons[$model->id] = [
                'primary_id' => $model->id,
                'first_name' => $model->first_name,
                'last_name' => $model->last_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $persons;
    }

    protected function addPhoneData($import, $model, $contains)
    {
        $key = implode('-', [$model->id, $model->phone]);
        if ($model && $model->phone && !$this->idExists('phones', $key)) {
            $this->addId('phones', $key);
            $phone = $this->normalizePhone($model->phone);
            if ($phone) {
                $contains[$phone] = [
                    'person_id' => $model->id,
                    'phone' => $phone,
                    'system' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'deleted_at' => null,
                ];
            }
        }
        return $contains;
    }

    protected function addTypeData($import, $model, $types)
    {
        if ($model && $model->type_id && !$this->idExists('types', $model->type_id)) {
            $this->addId('types', $model->type_id);
            $types[$model->type_id] = [
                'primary_id' => $model->type_id,
                'name' => $model->job_type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ];
        }
        return $types;
    }

    protected function createTempTables()
    {
        foreach ($this->tempTables as $method => $table) {
            if (method_exists($this, $method)) {
                $this->tempTables[$method] = $this->$method()->temp();
            }
        }
    }

    protected function dataOperationDept()
    {
        $this->dataOperationViaTemp(
            'departments',
            $this->getDept(),
            $this->tempTables['getDept'],
            [
                'primary_id',
            ],
            ['primary_id', 'name', 'deleted_at', 'created_at', 'updated_at'],
            ['name', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationEmail()
    {
        $this->dataOperationViaTemp(
            'emails',
            $this->getEmail(),
            $this->tempTables['getEmail'],
            [
                'person_id',
                'email',
            ],
            [
                'person_id',
                'email',
                'system',
                'deleted_at',
                'created_at',
                'updated_at',
            ],
            ['email', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationJob()
    {
        $this->dataOperationViaTemp(
            'jobs',
            $this->getJob(),
            $this->tempTables['getJob'],
            [
                'primary_id',
            ],
            [
                'primary_id',
                'type_id',
                'name',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['type_id', 'name', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationJobPerson()
    {
        $this->dataOperationViaTemp(
            'job_persons',
            $this->getJobPerson(),
            $this->tempTables['getJobPerson'],
            [
                'person_id',
                'department_id',
                'job_id',
                'location_id',
            ],
            [
                'person_id',
                'department_id',
                'job_id',
                'supervisor_id',
                'location_id',
                'manages',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['updated_at', 'supervisor_id', 'manages']
        );
    }

    protected function dataOperationLocation()
    {
        $this->dataOperationViaTemp(
            'locations',
            $this->getLocation(),
            $this->tempTables['getLocation'],
            [
                'primary_id',
            ],
            [
                'primary_id',
                'name',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['name', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationPerson()
    {
        $this->dataOperationViaTemp(
            'persons',
            $this->getPerson(),
            $this->tempTables['getPerson'],
            [
                'primary_id',
            ],
            [
                'primary_id',
                'first_name',
                'last_name',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['last_name', 'first_name', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationPhone()
    {
        $this->dataOperationViaTemp(
            'phones',
            $this->getPhone(),
            $this->tempTables['getPhone'],
            [
                'person_id',
                'phone',
            ],
            [
                'person_id',
                'phone',
                'system',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['phone', 'deleted_at', 'updated_at']
        );
    }

    protected function dataOperationType()
    {
        $this->dataOperationViaTemp(
            'types',
            $this->getType(),
            $this->tempTables['getType'],
            [
                'primary_id',
            ],
            [
                'primary_id',
                'name',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
            ['name', 'deleted_at', 'updated_at']
        );
    }

    protected function dropTempTables()
    {
        foreach ($this->tempTables as $method => $table) {
            if (method_exists($this, $method)) {
                $this->$method()->dropTemp();
            }
        }
    }

    protected function getChunk(\Smorken\Importer\Contracts\Models\Import $import, Collection $models)
    {
        $chunk = [
            'persons' => [],
            'locations' => [],
            'depts' => [],
            'types' => [],
            'jobs' => [],
            'dept_persons' => [],
            'job_persons' => [],
            'emails' => [],
            'phones' => [],
        ];
        foreach ($models as $model) {
            $chunk['persons'] = $this->addPersonData($import, $model, $chunk['persons']);
            $chunk['locations'] = $this->addLocationData($import, $model, $chunk['locations']);
            $chunk['depts'] = $this->addDeptData($import, $model, $chunk['depts']);
            $chunk['types'] = $this->addTypeData($import, $model, $chunk['types']);
            $chunk['jobs'] = $this->addJobData($import, $model, $chunk['jobs']);
            $chunk['job_persons'] = $this->addJobPersonData($import, $model, $chunk['job_persons']);
            $chunk['emails'] = $this->addEmailData($import, $model, $chunk['emails']);
            $chunk['phones'] = $this->addPhoneData($import, $model, $chunk['phones']);
        }
        return $chunk;
    }

    protected function getDept($return_provider = false)
    {
        return $this->getModel(Department::class, $return_provider);
    }

    protected function getEmail($return_provider = false)
    {
        return $this->getModel(Email::class, $return_provider);
    }

    protected function getJob($return_provider = false)
    {
        return $this->getModel(Job::class, $return_provider);
    }

    protected function getJobPerson($return_provider = false)
    {
        return $this->getModel(JobPerson::class, $return_provider);
    }

    protected function getLocation($return_provider = false)
    {
        return $this->getModel(Location::class, $return_provider);
    }

    protected function getPerson($return_provider = false)
    {
        return $this->getModel(Person::class, $return_provider);
    }

    protected function getPhone($return_provider = false)
    {
        return $this->getModel(Phone::class, $return_provider);
    }

    protected function getTableFromKey(string $table)
    {
        $method = $this->tableMap[$table] ?? null;
        if ($method && method_exists($this, $method)) {
            return $this->$method();
        }
        return null;
    }

    protected function getType($return_provider = false)
    {
        return $this->getModel(Type::class, $return_provider);
    }

    protected function handleDataOperations()
    {
        $this->dataOperationPerson();
        $this->dataOperationLocation();
        $this->dataOperationDept();
        $this->dataOperationType();
        $this->dataOperationJob();
        $this->dataOperationJobPerson();
        $this->dataOperationEmail();
        $this->dataOperationPhone();
    }

    protected function handleTempDataOperations($chunk)
    {
        foreach ($chunk as $table => $data) {
            $t = $this->getTableFromKey($table);
            if ($t) {
                $temp = sprintf('%s_temp', $t->getTable());
                $this->insertIntoTemp($table, $t, $temp, $data);
            }
        }
    }

    protected function idExists($key, $id)
    {
        return array_key_exists($id, $this->ids[$key]);
    }

    protected function loadSample(\Smorken\Importer\Contracts\Models\Import $import)
    {
        $chunk = [];
        $this->hrProvider->getActiveEmployees(
            function ($models) use ($import, &$chunk) {
                $chunk = $this->getChunk($import, $models);
            }
        );
        return $chunk;
    }

    protected function normalizePhone($phone)
    {
        $phone = preg_replace('/[^\d]/', '', $phone);
        if ($phone === '0000000000') {
            return null;
        }
        if (strlen($phone) === 10) {
            return preg_replace('/(\d{3})(\d{3})(\d{4})/', '($1) $2-$3', $phone);
        } elseif (strlen($phone) === 7) {
            return preg_replace('/(\d{3})(\d{4})/', '$1-$2', $phone);
        }
        return $phone;
    }
}
