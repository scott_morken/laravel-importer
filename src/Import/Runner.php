<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:23 AM
 */

namespace Smorken\Importer\Import;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\App;
use Smorken\Importer\Contracts\Models\Import;
use Smorken\Importer\Contracts\Models\ImportResult;
use Smorken\Importer\Contracts\Notify\Results;
use Smorken\Importer\Contracts\Storage\ImportRun;

class Runner implements \Smorken\Importer\Contracts\Import\Runner
{

    /**
     * @var DatabaseManager
     */
    protected $db;

    /**
     * @var ImportRun
     */
    protected $import_run_provider;

    /**
     * @var Results
     */
    protected $result_notifier;

    public function __construct(ImportRun $import_run_provider, Results $results_notifier, DatabaseManager $db)
    {
        $this->setImportRunProvider($import_run_provider);
        $this->setResultNotifier($results_notifier);
        $this->setDb($db);
    }

    /**
     * @return DatabaseManager
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param  DatabaseManager  $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return ImportRun
     */
    public function getImportRunProvider()
    {
        return $this->import_run_provider;
    }

    /**
     * @param  ImportRun  $import_run_provider
     */
    public function setImportRunProvider($import_run_provider)
    {
        $this->import_run_provider = $import_run_provider;
    }

    /**
     * @return Results
     */
    public function getResultNotifier()
    {
        return $this->result_notifier;
    }

    /**
     * @param  Results  $result_notifier
     */
    public function setResultNotifier($result_notifier)
    {
        $this->result_notifier = $result_notifier;
    }

    /**
     * @param  Import  $import
     * @return ImportResult
     */
    public function run(Import $import)
    {
        $provider = $this->createImportProvider($import);
        if ($provider) {
            $provider->init($this->getImportRunProvider()->getResultModel(true), $this->getDb());
            $result = $provider->import($import);
            $run = $this->saveResult($import, $result);
            $this->notify($import, $run);
            return $result;
        }
    }

    public function sample(Import $import)
    {
        $provider = $this->createImportProvider($import);
        if ($provider) {
            $provider->init($this->getImportRunProvider()->getResultModel(true), $this->getDb());
            return $provider->sample($import);
        }
        return [];
    }

    protected function createImportProvider(Import $import)
    {
        $ip = $import->importProvider;
        $provider_cls = $ip ? $ip->provider : null;
        if ($provider_cls) {
            return App::make($provider_cls, $ip->params ?? []);
        }
        return null;
    }

    protected function notify(Import $import, \Smorken\Importer\Contracts\Models\ImportRun $run)
    {
        $this->getResultNotifier()->send($import, $run);
    }

    protected function saveResult(Import $import, ImportResult $result)
    {
        return $this->getImportRunProvider()->saveByImportAndResult($import, $result);
    }
}
