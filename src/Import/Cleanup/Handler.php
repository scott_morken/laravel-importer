<?php


namespace Smorken\Importer\Import\Cleanup;


use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\Importer\Contracts\Import\CleanupHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler implements CleanupHandler
{

    protected $types = [];

    public function __construct(array $types)
    {
        $this->types = $types;
    }

    public function addType($key, $concrete)
    {
        if (!isset($this->types[$key])) {
            $this->types[$key] = new Collection();
        }
        $this->types[$key]->push($concrete);
    }

    public function getType($key)
    {
        $key = Str::start($key, 'cleanup.');
        return Arr::get($this->types, $key);
    }

    public function run($type, $force = false)
    {
        $type = $this->getType($type);
        if ($type && count($type)) {
            $results = [];
            foreach ($type as $t) {
                $results[] = $t->cleanup($force);
            }
            return $results;
        }
        throw new NotFoundHttpException("Type [$type] could not be located.");
    }

    public function runById($type, $id, $force = false)
    {
        $types = $this->getType($type);
        $provider = $types[$id] ?? null;
        if ($provider) {
            return $provider->cleanup($force);
        }
        throw new NotFoundHttpException("Type [$type] with ID [$id] could not be located.");
    }

    public function setAttribute($type, $key, $value)
    {
        $types = $this->getType($type);
        if ($types && count($types)) {
            foreach ($types as $t) {
                $t->setAttribute($key, $value);
            }
        }
    }

    public function setAttributeById($type, $id, $key, $value)
    {
        $types = $this->getType($type);
        $provider = $types[$id] ?? null;
        if ($provider) {
            return $provider->setAttribute($key, $value);
        }
    }

    public function setCompDate($type, $date = '-14 days')
    {
        $types = $this->getType($type);
        if ($types && count($types)) {
            foreach ($types as $t) {
                $t->setCompDate($date);
            }
        }
    }

    public function setCompDateById($type, $id, $date = '-14 days')
    {
        $types = $this->getType($type);
        $provider = $types[$id] ?? null;
        if ($provider) {
            return $provider->setCompDate($date);
        }
    }
}
