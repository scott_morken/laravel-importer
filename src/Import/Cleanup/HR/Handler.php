<?php


namespace Smorken\Importer\Import\Cleanup\HR;


use Smorken\Importer\Contracts\Storage\HR\Department;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\Job;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Location;
use Smorken\Importer\Contracts\Storage\HR\Person;
use Smorken\Importer\Contracts\Storage\HR\Phone;
use Smorken\Importer\Contracts\Storage\HR\Type;
use Smorken\Importer\Import\Cleanup\Cleanup;

class Handler extends Cleanup implements \Smorken\Importer\Contracts\Import\Cleanup
{

    protected function buildQueries($comp_date, $force = false)
    {
        $this->peopleRelatedQueries($comp_date, $force);
        $this->typeRelatedQueries($comp_date, $force);
        $this->jobRelatedQueries($comp_date, $force);
        $this->departmentRelatedQueries($comp_date, $force);
        $this->locationRelatedQueries($comp_date, $force);
    }

    protected function departmentRelatedQueries($date, $force = false)
    {
        $provider_class = Department::class;
        $ids = $this->createCoreQuery($provider_class, $date, $force);
        $this->createRelatedQuery(JobPerson::class, 'department_id', $ids, $force);
    }

    protected function jobRelatedQueries($date, $force = false)
    {
        $provider_class = Job::class;
        $ids = $this->createCoreQuery($provider_class, $date, $force);
        $this->createRelatedQuery(JobPerson::class, 'job_id', $ids, $force);
    }

    protected function locationRelatedQueries($date, $force = false)
    {
        $provider_class = Location::class;
        $ids = $this->createCoreQuery($provider_class, $date, $force);
        $this->createRelatedQuery(JobPerson::class, 'location_id', $ids, $force);
    }

    protected function modifyQuery($type, $key, $query)
    {
        $type_id = $this->getAttribute('type_id');
        if ($type_id && $key === Job::class) {
            $query->where('type_id', '=', $type_id);
        }
        return $query;
    }

    protected function peopleRelatedQueries($date, $force = false)
    {
        $provider_class = Person::class;
        $ids = $this->createCoreQuery($provider_class, $date, $force);
        $this->createRelatedQuery(JobPerson::class, 'person_id', $ids, $force);
        $this->createRelatedQuery(Email::class, 'person_id', $ids, $force);
        $this->createRelatedQuery(Phone::class, 'person_id', $ids, $force);
    }

    protected function typeRelatedQueries($date, $force = false)
    {
        $provider_class = Type::class;
        $ids = $this->createCoreQuery($provider_class, $date, $force);
        $this->createRelatedQuery(Job::class, 'type_id', $ids, $force);
    }
}
