<?php


namespace Smorken\Importer\Import\Cleanup\HR;


use Smorken\Importer\Contracts\Import\Cleanup;
use Smorken\Importer\Contracts\Storage\HR\Email;
use Smorken\Importer\Contracts\Storage\HR\JobPerson;
use Smorken\Importer\Contracts\Storage\HR\Phone;

class Orphans extends \Smorken\Importer\Import\Cleanup\Orphans implements Cleanup
{

    protected function buildQueries($comp_date, $force)
    {
        $this->createRelationQuery(JobPerson::class, ['person', 'job', 'department', 'location'], $force);
        $this->createRelationQuery(Phone::class, ['person'], $force);
        $this->createRelationQuery(Email::class, ['person'], $force);
    }
}
