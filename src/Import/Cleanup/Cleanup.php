<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/28/17
 * Time: 9:35 AM
 */

namespace Smorken\Importer\Import\Cleanup;

use Illuminate\Support\Facades\App;

abstract class Cleanup implements \Smorken\Importer\Contracts\Import\Cleanup
{

    protected $attributes = [
        'comp_date' => '-14 days',
    ];

    protected $providers = [];

    protected $queries = [
        'forceDelete' => [],
        'delete' => [],
    ];

    protected $results = [
        'stale' => [],
        'forceDelete' => [],
        'delete' => [],
    ];

    public function __construct($look_back = '-14 days')
    {
        $this->setCompDate($look_back);
    }

    abstract protected function buildQueries($comp_date, $force);

    public function cleanup($force = false)
    {
        $this->createQueries($force);
        $this->runQueries();
        return $this->results;
    }

    public function getAttribute($key, $default = null)
    {
        return $this->attributes[$key] ?? $default;
    }

    public function getProvider($contract, $return_model = false)
    {
        if (!isset($this->providers[$contract])) {
            $provider = App::make($contract);
            $this->providers[$contract] = $provider;
        }
        $provider = $this->providers[$contract];
        if ($return_model && $provider) {
            return $provider->getModel();
        }
        return $provider;
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function setCompDate($date = '-14 days')
    {
        $this->setAttribute('comp_date', date('Y-m-d H:i:s', strtotime($date)));
    }

    public function setProvider($contract, $instance)
    {
        $this->providers[$contract] = $instance;
    }

    public function setProviders($providers)
    {
        foreach ($providers as $contract => $inst) {
            $this->setProvider($contract, $inst);
        }
    }

    protected function addDeleteQuery($key, $query)
    {
        $this->addQuery('delete', $key, $query);
    }

    protected function addForceDeleteQuery($key, $query)
    {
        $this->addQuery('forceDelete', $key, $query);
    }

    protected function addQuery($type, $key, $query)
    {
        $this->queries[$type][$key][] = $this->modifyQuery($type, $key, $query);
        $this->results[$type][$key] = 0;
    }

    protected function createCoreQuery($provider_class, $date, $force = false)
    {
        $stale_ids = $this->getStaleIds($provider_class, $date);
        $q = $this->getStaleQuery($provider_class, $stale_ids, $force);
        if ($force && !method_exists($q, 'forceDelete')) {
            $force = false;
        }
        $this->addQuery(
            $force ? 'forceDelete' : 'delete',
            $provider_class,
            $q
        );
        return $stale_ids;
    }

    protected function createQueries($force = false)
    {
        $comp_date = $this->getAttribute('comp_date');
        if ($comp_date) {
            $this->buildQueries($comp_date, $force);
        }
    }

    protected function createRelatedQuery($provider_class, $column, $ids, $force = false)
    {
        $m = $this->getProvider($provider_class, true);
        $this->disableTimestamps($m);
        $query = $m->newQuery()
                   ->whereIn($column, $ids);
        if ($force) {
            $force = $this->enableTrashed($query, $m);
        }
        $this->addQuery(
            $force ? 'forceDelete' : 'delete',
            $provider_class,
            $query
        );
    }

    protected function disableTimestamps($model)
    {
        if (property_exists($model, 'timestamps')) {
            $model->timestamps = false;
        }
    }

    protected function enableTrashed($query, $model)
    {
        if (method_exists($model, 'trashed')) {
            $query->withTrashed();
            return true;
        }
        return false;
    }

    protected function getStaleIds($provider_class, $date)
    {
        $m = $this->getProvider($provider_class, true);
        $query = $m->newQuery()
                   ->where('updated_at', '<', $date);
        $this->modifyQuery('stale_ids', $provider_class, $query);
        $this->enableTrashed($query, $m);
        $coll = $query->get()->pluck($m->getKeyName());
        $this->results['stale'][$provider_class] = count($coll);
        return $coll->all();
    }

    protected function getStaleQuery($provider_class, $ids, $force = false)
    {
        $m = $this->getProvider($provider_class, true);
        $this->disableTimestamps($m);
        $query = $m->newQuery()
                   ->whereIn($m->getKeyName(), $ids);
        $this->enableTrashed($query, $m);
        return $query;
    }

    protected function modifyQuery($type, $key, $query)
    {
        return $query;
    }

    protected function runQueries()
    {
        foreach ($this->queries as $type => $provider_queries) {
            foreach ($provider_queries as $provider_class => $pqs) {
                foreach ($pqs as $query) {
                    $this->results[$type][$provider_class] = $this->results[$type][$provider_class] + $query->$type();
                }
            }
        }
    }
}
