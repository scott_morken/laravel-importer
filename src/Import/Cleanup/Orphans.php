<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/28/17
 * Time: 9:36 AM
 */

namespace Smorken\Importer\Import\Cleanup;

abstract class Orphans extends Cleanup implements \Smorken\Importer\Contracts\Import\Cleanup
{

    protected function createQueries($force = false)
    {
        $this->buildQueries(null, $force);
    }

    protected function createRelationQuery($provider_class, $relations, $force = false)
    {
        $m = $this->getProvider($provider_class, true);
        $this->disableTimestamps($m);
        $query = $m->newQuery()
                   ->where(
                       function ($q) use ($relations) {
                           foreach ($relations as $r) {
                               $q->orHas($r, '=', 0);
                           }
                       }
                   );
        if ($force) {
            $force = $this->enableTrashed($query, $m);
        }
        $this->addQuery(
            $force ? 'forceDelete' : 'delete',
            $provider_class,
            $query
        );
    }
}
